#include <stdio.h>
#include <iostream>
 
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std; 
using namespace cv;
 
// Global variables

int thresh = 200;
int max_thresh = 255;
 Mat image1;
 Mat image2;
 Mat gray_image1;
 Mat gray_image2;

/// Function header
void cornerHarris_demo( int, void* );

void readme();
 
/** @function main */
int main( int argc, char** argv )
{
 if( argc != 3 )
 { readme(); return -1; }
 
// Load the images
 image1= imread( argv[2] );
 image2= imread( argv[1] );
cout << "read images" << endl;
 gray_image1;
 gray_image2;
Size size(518,518);//the dst image size,e.g.100x100
//resize(image1,image1,size);//res
//resize(image2,image2,size);

// Convert to Grayscale
cvtColor( image1, gray_image1, CV_RGB2GRAY );
cvtColor( image2, gray_image2, CV_RGB2GRAY );
cout << "converted to gray" << endl; 

//imshow("first image",image2);
//imshow("second image",image1);
cout << "display images" << endl;

/*
// ********************************* Stitch with SURF + RANSAC *******************
if( !gray_image1.data || !gray_image2.data )
 { std::cout<< " --(!) Error reading images " << std::endl; return -1; }
 
//-- Step 1: Detect the keypoints using SURF Detector
 int minHessian = 400;
 
SurfFeatureDetector detector( minHessian );
 
std::vector< KeyPoint > keypoints_object, keypoints_scene;
 
detector.detect( gray_image1, keypoints_object );
detector.detect( gray_image2, keypoints_scene );
 
//-- Step 2: Calculate descriptors (feature vectors)
SurfDescriptorExtractor extractor;
 
Mat descriptors_object, descriptors_scene;
 
extractor.compute( gray_image1, keypoints_object, descriptors_object );
extractor.compute( gray_image2, keypoints_scene, descriptors_scene );
 
//-- Step 3: Matching descriptor vectors using FLANN matcher
 FlannBasedMatcher matcher;
 std::vector< DMatch > matches;
 matcher.match( descriptors_object, descriptors_scene, matches );
 
double max_dist = 0; double min_dist = 100;
 
//-- Quick calculation of max and min distances between keypoints
 for( int i = 0; i < descriptors_object.rows; i++ )
 { double dist = matches[i].distance;
 if( dist < min_dist ) min_dist = dist;
 if( dist > max_dist ) max_dist = dist;
 }
 
 printf("-- Max dist : %f \n", max_dist );
 printf("-- Min dist : %f \n", min_dist );
 

//-- Use only "good" matches (i.e. whose distance is less than 3*min_dist )
 std::vector< DMatch > good_matches;
 
for( int i = 0; i < descriptors_object.rows; i++ )
 { if( matches[i].distance < 3*min_dist )
 { good_matches.push_back( matches[i]); }
 }
 std::vector< Point2f > obj;
 std::vector< Point2f > scene;
 
for( int i = 0; i < good_matches.size(); i++ )
 {
 //-- Get the keypoints from the good matches
 obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
 scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
 }
 
// Find the Homography Matrix
 Mat H = findHomography( obj, scene, CV_RANSAC );
 // Use the Homography Matrix to warp the images
 cv::Mat result;
 warpPerspective(image1,result,H,cv::Size(image1.cols+image2.cols,image1.rows));
 cv::Mat half(result,cv::Rect(0,0,image2.cols,image2.rows));
 image2.copyTo(half);
 imshow( "Result", result );
 // *******************************************************************************

*/



/*
// ********************* Harris Feature Detector ********************************* //

 /// Create a window and a trackbar
  namedWindow( "source", CV_WINDOW_AUTOSIZE );
  createTrackbar( "Threshold: ", "source", &thresh, max_thresh, cornerHarris_demo );
  imshow( "source", image1 );

  cornerHarris_demo( 0, 0 );

// ******************************************************************************* //

*/


// ****************************** stitch with harris **************************** //

cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create("HARRIS");
cv::Ptr<cv::DescriptorExtractor> descriptor = cv::DescriptorExtractor::create("SIFT"); 


// detect keypoints
std::vector<cv::KeyPoint> keypoints1, keypoints2;
detector->detect(gray_image1, keypoints1);
detector->detect(gray_image2, keypoints2);
cout << "detected using harris " << keypoints1.size() << " " << keypoints2.size() << endl;

 //-- Step 2: Calculate descriptors (feature vectors)
cv::Mat descriptors1, descriptors2;
SurfDescriptorExtractor extractor;


// using surf descriptor
  extractor.compute( image1, keypoints1, descriptors1 );
  extractor.compute( image2, keypoints2, descriptors2 );
cout << "success with surfDescriptor" << endl;

/*
// using sift descriptor
descriptor->compute(gray_image1, keypoints1, descriptors1);
descriptor->compute(gray_image2, keypoints2, descriptors2);
cout << "success with siftDescriptor" << endl;
*/



//-- Step 3: Matching descriptor vectors using FLANN matcher
 FlannBasedMatcher matcher;
 std::vector< DMatch > matches;
 matcher.match( descriptors1, descriptors2, matches );
 
double max_dist = 0; double min_dist = 100;
 
//-- Quick calculation of max and min distances between keypoints
 for( int i = 0; i < descriptors1.rows; i++ )
 { double dist = matches[i].distance;
 if( dist < min_dist ) min_dist = dist;
 if( dist > max_dist ) max_dist = dist;
 }
 
 printf("-- Max dist : %f \n", max_dist );
 printf("-- Min dist : %f \n", min_dist );
 

//-- Use only "good" matches (i.e. whose distance is less than 3*min_dist )
 std::vector< DMatch > good_matches;
 int count = 0;
for( int i = 0; i < descriptors1.rows; i++ )
 { if( matches[i].distance < 3*min_dist )
   { 
    good_matches.push_back( matches[i]); 
    count ++;
   }
 }

 cout << "found good matches: " << good_matches.size() << " " << count << endl;
 std::vector< Point2f > selected_points1;
 std::vector< Point2f > selected_points2;
 
for( int i = 0; i < good_matches.size(); i++ )
 {
 //-- Get the keypoints from the good matches
 selected_points1.push_back( keypoints1[ good_matches[i].queryIdx ].pt );
 circle( image1, Point( keypoints1[ good_matches[i].queryIdx ].pt.x, keypoints1[ good_matches[i].queryIdx ].pt.y ), 5,  Scalar( 0, 255, 255 ), 2, 8, 0 );
 selected_points2.push_back( keypoints2[ good_matches[i].trainIdx ].pt );
 circle( image2, Point( keypoints2[ good_matches[i].queryIdx ].pt.x, keypoints2[ good_matches[i].queryIdx ].pt.y ), 5,  Scalar( 0, 255, 255 ), 2, 8, 0 );
 }

imshow("first image",image2);
imwrite("/home/sanka/TestImages/harris1.jpg" , image2);
imshow("second image",image1);
imwrite("/home/sanka/TestImages/harris2.jpg" , image1);

// Find the Homography Matrix
 Mat H = findHomography( selected_points1, selected_points2, CV_RANSAC );

Mat image_matches;
drawMatches( image1, keypoints1, image2, keypoints2,
               good_matches, image_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
imwrite("/home/sanka/TestImages/harris_homography.jpg" , image_matches);

 // Use the Homography Matrix to warp the images
 cv::Mat result;
 warpPerspective(image1,result,H,cv::Size(image1.cols+image2.cols,image1.rows));
 cv::Mat half(result,cv::Rect(0,0,image2.cols,image2.rows));
 image2.copyTo(half);
 imwrite( "/home/sanka/TestImages/harrisResult.jpg" , result);
 
 imshow( "Result", result );
// ********************************************************************************* //

 waitKey(0);
 return 0;
 }
 
/** @function readme */
 void readme()
 { std::cout << " Usage: Panorama < img1 > < img2 >" << std::endl; }

/** @function cornerHarris_demo */
void cornerHarris_demo( int, void* )
{

  Mat dst, dst_norm, dst_norm_scaled;
  dst = Mat::zeros( image1.size(), CV_32FC1 );

  /// Detector parameters
  int blockSize = 2;
  int apertureSize = 3;
  double k = 0.04;

  /// Detecting corners
  cornerHarris( gray_image1, dst, blockSize, apertureSize, k, BORDER_DEFAULT );

  /// Normalizing
  normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
  convertScaleAbs( dst_norm, dst_norm_scaled );

  /// Drawing a circle around corners
  for( int j = 0; j < dst_norm.rows ; j++ )
     { for( int i = 0; i < dst_norm.cols; i++ )
          {
            if( (int) dst_norm.at<float>(j,i) > thresh )
              {
               circle( dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
              }
          }
     }



  /// Showing the result
namedWindow( "harris Result", CV_WINDOW_AUTOSIZE );
  imshow( "harris Result", dst_norm_scaled );
}

/*


#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: DisplayImage.out <Image_Path>\n");
        return -1;
    }

    Mat image;
    image = imread( argv[1], 1 );

    if ( !image.data )
    {
        printf("No image data \n");
        return -1;
    }
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    imshow("Display Image", image);

    waitKey(0);

    return 0;
}

*/
