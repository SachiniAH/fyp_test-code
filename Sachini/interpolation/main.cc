#include "intermediate.h"
#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

char *img1;
char *img2;
char *imgDst;
string name_map[] = {"posy", "negz", "negx", "posz", "posx", "negy"};

void stitch(double ratio, string name, int dir) {
    Mat dst, src1, src2;
    src1 = imread(img1 + name + ".png");
    src2 = imread(img2 + name + ".png");
    if (!src1.data) {
        cout << "Error loading src1" << name << endl ;
        return;
    }
    if (!src2.data) {
        cout << "Error loading src2 " << name << endl;
        return;
    }

    // Fast Stitching
    vector<float> shifts;
    Mat stitched_CV;
    if( dir ==1) {
        shifts = alignment(src1, src2);
        stitched_CV = fastStitching(src1, src2, shifts);

        int w = stitched_CV.cols, h = stitched_CV.rows;
        int w_o = src1.cols, h_o = src1.rows;

        int w_d = (int) (w - w_o)*ratio;
        Mat dst = stitched_CV(Rect(w_d,0,w_o,h_o));
        imwrite(imgDst + name + ".png", dst);
    } else {
        shifts = alignment(src2, src1);
        stitched_CV = fastStitching(src1, src2, shifts);

        int w = stitched_CV.cols, h = stitched_CV.rows;
        int w_o = src1.cols, h_o = src1.rows;

        int w_d = (int) (w - w_o)*ratio;
        Mat dst = stitched_CV(Rect(w_d,0,w_o,h_o));
        imwrite(imgDst + name + ".png", dst);
    }


    /*Stitcher stitcher = Stitcher::createDefault();
    vector<Mat> inputImg;
    if( dir ==1) {
        inputImg = {src1, src2};
    } else {
        inputImg = {src2, src1};
    }
    Mat stitched_CV;
    Stitcher::Status status = stitcher.stitch(inputImg, stitched_CV);

    if (Stitcher::OK == status) {
        int w = stitched_CV.cols, h = stitched_CV.rows;
        int w_o = src1.cols, h_o = src1.rows;

        int w_d = (int) (w - w_o)*ratio;
        Mat dst = stitched_CV(Rect(w_d,0,w_o,h_o));
        imwrite(imgDst + name + ".png", stitched_CV);
    }
    else {
        printf("Stitching fail.");
    }*/
}

void interpolate(double ratio, string name, int dir){
    Mat dst, src1, src2;
    src1 = imread(img1 + name + ".png");
    src2 = imread(img2 + name + ".png");
    if (!src1.data) {
        cout <<"Error loading src1 " << name << endl;
        return;
    }
    if (!src2.data) {
        cout << "Error loading src2 "<< name << endl;
        return;
    }

    if(dir==1){
        addWeighted(src1, ratio, src2, 1-ratio, 0.0, dst);
    } else {
        addWeighted(src2, ratio, src1, 1-ratio, 0.0, dst);
    }

    imwrite(imgDst + name + ".png", dst);
}

int main(int argc, char **argv) {
    std::cout << " Simple Linear Blender " << std::endl;
    std::cout << "-----------------------" << std::endl;

    img1 = argv[1];
    img2 = argv[2];
    imgDst = argv[3];
    double ratio = atof(argv[4]);

    interpolate(ratio,name_map[1], -1 );
    interpolate(ratio, name_map[3], 1);
    stitch(ratio, name_map[2], 1);
    stitch(ratio, name_map[4], -1);

    return 0;
}

