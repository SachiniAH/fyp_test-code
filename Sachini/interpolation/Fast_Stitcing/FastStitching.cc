#include <stdio.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

// Global variables

Mat panaroma;
Mat* images;
int* overlap;
Mat image1;
Mat image2;
int img_width;
int img_height;
int overlap_start_x;
int im2_start_y;
int im1_start_y;
double gamma_Value = 2.2;
void readme();

void colourCorrect();

Mat correctGamma( Mat& img);

Mat fastStitching(Mat _image1, Mat _image2, vector<float> overlap_percentages) {


    //image1 = imread(argv[1]);
    //image2 = imread(argv[2]);

    //String result_name = argv[argc-1];

    // Load and resize images
    /* Size size(750, 1000);
    resize(imread(argv[1]), image1,size);
    resize(imread(argv[2]),image2,size);*/

    image1 = _image1;
    image2 = _image2;

    double overlap_x = overlap_percentages[0];
    double overlap_y = overlap_percentages[1];

    img_width = (int)((image1.cols * overlap_x) / 100);
    img_height = abs((int)((image1.rows * overlap_y) / 100));
    overlap_start_x = image1.cols - img_width;

    // first overlapping row
    im1_start_y = overlap_y > 0 ? image1.rows - img_height : 0;
    im2_start_y = overlap_y > 0 ? 0 : image2.rows - img_height;

    // first non-overlapping row
    int im1_start_no = overlap_y > 0 ?  0 : img_height;
    int im2_start_no = overlap_y > 0 ?  img_height : 0 ;

    // first non-overlapping row in merged image
    int im1_merge_start = overlap_y > 0 ?  0 : image2.rows ;
    int im2_merge_start = overlap_y > 0 ?  image1.rows : 0 ;

    // first overlapping row in merged image
    int olap_start_y = overlap_y > 0 ?  image1.rows - img_height : image2.rows - img_height ;


    //imshow("first image", image1);
    //imshow("second image", image2);

    //image1 = correctGamma(image1);
    //image2 = correctGamma(image2);

    //colourCorrect();

    int **error_surface = new int *[img_height];
    for (int i = 0; i < img_height; ++i) {
        error_surface[i] = new int[img_width];
    }
    //int[][] error_surface = new int[overlap_start_x][gray_image1.cols];

    // calculate error surface
    for (int j = 0; j < img_height; j++) {
        for (int i = 0; i < img_width; i++) {
            /*error_surface[j][i]  = pow(gray_image1.at<uchar>(j, overlap_start_x + i) - gray_image2.at<uchar>(j, i), 2);*/
            error_surface[j][i] = pow(image1.at<Vec3b>(im1_start_y + j, overlap_start_x + i)[0] - image2.at<Vec3b>(im2_start_y + j, i)[0], 2);
            error_surface[j][i] += pow(image1.at<Vec3b>(im1_start_y + j, overlap_start_x + i)[1] - image2.at<Vec3b>(im2_start_y + j, i)[1], 2);
            error_surface[j][i] += pow(image1.at<Vec3b>(im1_start_y + j, overlap_start_x + i)[2] - image2.at<Vec3b>(im2_start_y + j, i)[2], 2);
        }
    }

    // calculate cumilative errors to find optimal seam
    int **cum_min = new int *[img_height];
    cum_min[0] = error_surface[0];
    for (int j = 1; j < img_height; ++j) {
        cum_min[j] = new int[img_width];

        int i = 0;
        cum_min[j][i] = error_surface[j][i] + min(cum_min[j - 1][i], cum_min[j - 1][i + 1]);

        for (i = 1; i < img_width - 1; ++i) {
            cum_min[j][i] =
                    error_surface[j][i] + min(min(cum_min[j - 1][i - 1], cum_min[j - 1][i]), cum_min[j - 1][i + 1]);
        }
        cum_min[j][i] = error_surface[j][i] + min(cum_min[j - 1][i - 1], cum_min[j - 1][i]);
    }

    // create new image
    cv::Mat mergeMat = cv::Mat(image1.rows + image2.rows - img_height, image2.cols + image1.cols - img_width, image2.type());

    // Create non-overlapping part of left image
    for (int j = 0; j < image1.rows - img_height ; j++) {
        for (int i = 0; i < image1.cols ; ++i) {
            mergeMat.at<Vec3b>(im1_merge_start + j, i) = image1.at<Vec3b>(im1_start_no + j, i);
        }
    }

    // Create non-overlapping part of right image
    for (int j = 0; j < image2.rows - img_height ; j++) {
        for (int i = 0; i < image2.cols ; ++i) {
            mergeMat.at<Vec3b>(im2_merge_start + j, overlap_start_x +i) = image2.at<Vec3b>(im2_start_no + j, i);
        }
    }

    // Create overlapping region
    for (int j = 0; j < img_height ; j++) {
        int min = 0;

        for (int i = 1; i < img_width; ++i) {           // find the minimum
            if (cum_min[j][i] < cum_min[j][min]) {
                min = i;
            }
        }

        int i;
        for (i = 0; i < overlap_start_x + min; ++i) {           // overlapping rows from image left
            mergeMat.at<Vec3b>(olap_start_y + j, i) = image1.at<Vec3b>(im1_start_y + j, i);
        }

        // Print optimal seam
        /*mergeMat.at<Vec3b>(olap_start_y+j, i-5) = Vec3b(0, 0, 255);
        mergeMat.at<Vec3b>(olap_start_y+j, i-4) = Vec3b(0, 0, 255);
        mergeMat.at<Vec3b>(olap_start_y+j, i-3) = Vec3b(0, 0, 255);
        mergeMat.at<Vec3b>(olap_start_y+j, i-2) = Vec3b(0, 0, 255);
        mergeMat.at<Vec3b>(olap_start_y+j, i-1) = Vec3b(0, 0, 255);*/

        for (int k = min; k < image2.cols; ++k, ++i) {      // overlapping rows from image right
            mergeMat.at<Vec3b>(olap_start_y + j, i) = image2.at<Vec3b>(im2_start_y + j, k);
        }
    }

    //mergeMat = correctGamma(mergeMat);

    //imwrite(result_name, mergeMat);

    //imshow("result", mergeMat);
    //waitKey(0);
    return mergeMat;
}

void colourCorrect() {
    double coeff1[] = {0, 0, 0};
    double coeff2[] = {0, 0, 0};
    Vec3b color;
    double *g = new double[3];
    double *im2coeff = new double[3];

    // Local colour coefficients for img1
    for (int j = 0; j < img_height; j++) {
        for (int i = 0; i < img_width; i++) {
            color = image1.at<Vec3b>(im1_start_y + j, overlap_start_x + i);
            coeff1[0] += pow(color[0], gamma_Value);
            coeff1[1] += pow(color[1], gamma_Value);
            coeff1[2] += pow(color[2], gamma_Value);
        }
    }

    // Local colour coefficients for img2
    for (int j = 0; j < img_height; j++) {
        for (int i = 0; i < img_width; i++) {
            color = image2.at<Vec3b>(j, i);
            coeff2[0] += pow(color[0], gamma_Value);
            coeff2[1] += pow(color[1], gamma_Value);
            coeff2[2] += pow(color[2], gamma_Value);
        }
    }

    // Colour coefficient for image2 w.r.t image1
    im2coeff[0] = coeff1[0] / coeff2[0];
    im2coeff[1] = coeff1[1] / coeff2[1];
    im2coeff[2] = coeff1[2] / coeff2[2];

    // Global Colour coefficient
    g[0] = (1 + im2coeff[0]) / (1 + pow(im2coeff[0], 2));
    g[1] = (1 + im2coeff[1]) / (1 + pow(im2coeff[1], 2));
    g[2] = (1 + im2coeff[2]) / (1 + pow(im2coeff[2], 2));

    /*cout << g[0] << " " << g[1] << " " << g[2] << " " << endl;
    cout << im2coeff[0] << " " << im2coeff[1] << " " << im2coeff[2] << " " << endl;*/

    double factor;

    // colour correct image1
    for (int k = 0; k < 3; ++k) {
        factor = pow(g[k], 1 / gamma_Value);
        //cout << factor << endl;
        for (int j = 0; j < image1.rows; j++) {
            for (int i = 0; i < image1.cols; i++) {
                image1.at<Vec3b>(j, i)[k] = min((int)round(factor * image1.at<Vec3b>(j, i)[k]), 255);
            }
        }
    }

    // colour correct image2
    //cout << pow( 1.00575, 1/gamma_Value) << " " << 1/gamma_Value << endl;
    for (int k = 0; k < 3; ++k) {
        factor = pow(g[k] * im2coeff[k], 1 / gamma_Value);
        //cout << factor << endl;
        //cout << g[k]  << "  " << coeff2[k] << " " << pow(g[k] * coeff2[k], 1 / gamma_Value) << " " << 1 / gamma_Value << endl;
        for (int j = 0; j < image2.rows; j++) {
            for (int i = 0; i < image2.cols; i++) {
                image2.at<Vec3b>(j, i)[k] =  min((int)round(factor * image2.at<Vec3b>(j, i)[k]),255);
            }
        }
    }
}


// Gamma correction for the specified image
Mat correctGamma( Mat& img) {
    double inverse_gamma = 1.0 / gamma_Value;

    Mat lut_matrix(1, 256, CV_8UC1 );
    uchar * ptr = lut_matrix.ptr();
    for( int i = 0; i < 256; i++ )
        ptr[i] = (int)( pow( (double) i / 255.0, inverse_gamma ) * 255.0 );

    Mat result;
    LUT( img, lut_matrix, result );

    return result;
}