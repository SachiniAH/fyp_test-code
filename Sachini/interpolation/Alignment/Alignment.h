//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#ifndef FYP_LIBRARY_ALIGNMENT_H
#define FYP_LIBRARY_ALIGNMENT_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

vector<float> alignment( Mat img1, Mat img2 );

#endif //FYP_LIBRARY_ALIGNMENT_H
