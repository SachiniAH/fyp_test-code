\documentclass[conference]{IEEEtran}
\usepackage{blindtext, graphicx}

% *** GRAPHICS RELATED PACKAGES ***
\ifCLASSINFOpdf
  % \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  \DeclareGraphicsExtensions{.pdf,.eps}
\else
  %\usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% *** MATH PACKAGES ***
%
\usepackage{amsmath}
\usepackage{fixltx2e}
\usepackage{hyperref}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Quantitative and Qualitative Evaluation of Performance and Robustness of Image Stitching Algorithms}

% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
%\author{
%\IEEEauthorblockN{Vipula Dissanayake}
%\IEEEauthorblockA{
%vipula.11@cse.mrt.ac.lk}
%\and
%\IEEEauthorblockN{Sachini Herath}
%\IEEEauthorblockA{
%sachini.11@cse.mrt.ac.lk}
%\and
%\IEEEauthorblockN{Sanka Rasnayaka}
%\IEEEauthorblockA{
%Email: sachini.11@cse.mrt.ac.lk}
%\and
%\IEEEauthorblockN{Rajith Vidanaarachchi}
%\IEEEauthorblockA{
%Email: raji.11@cse.mrt.ac.lk}
%\and
%\IEEEauthorblockN{Sachith Seneviratne}
%\IEEEauthorblockA{
%Email: sachith.11@cse.mrt.ac.lk}

%}

\author{
\IEEEauthorblockN{Vipula Dissanayake\IEEEauthorrefmark{1},
Sachini Herath\IEEEauthorrefmark{2},
Sanka Rasnayaka\IEEEauthorrefmark{3}, 
Sachith Seneviratne\IEEEauthorrefmark{4} \\
Rajith Vidanaarachchi\IEEEauthorrefmark{5} and
Chandana Gamage\IEEEauthorrefmark{6}}
\IEEEauthorblockA{Department of Computer Science \& Engineering,
University of Moratuwa \\
Sri Lanka\\
Email: \IEEEauthorrefmark{1}vipula.11@cse.mrt.ac.lk,
\IEEEauthorrefmark{2}sachini.11@cse.mrt.ac.lk,
\IEEEauthorrefmark{3}sanka.11@cse.mrt.ac.lk, 
\IEEEauthorrefmark{4}sachith.11@cse.mrt.ac.lk, \\
\IEEEauthorrefmark{5}raji.11@cse.mrt.ac.lk, 
\IEEEauthorrefmark{6}chandag@cse.mrt.ac.lk }}

\maketitle

\begin{abstract}
%\boldmath
Many different image stitching algorithms, and mechanisms to assess their quality have been proposed by different research groups in the past decade. However, a comparison across different stitching algorithms and evaluation mechanisms has not been performed before. Our objective is to recognize the best algorithm for panoramic image stitching. We measure the robustness of different algorithms by means of assessing image quality of a set of panoramas. For the evaluation itself a varied set of assessment criteria are used, and the evaluation is performed over a large range of images captured using differing cameras. In an ideal stitching algorithm, the resulting stitched image should be without visible seams and other noticeable anomalies. An objective evaluation for image quality should give results corresponding to a similar evaluation by the Human Visual System. Our conclusion is that the choice of stitching algorithm is scenario dependent, with run-time and accuracy being the primary considerations.
\end{abstract}

\begin{IEEEkeywords}
Panoramic Stitching, Image Stitching, Quality Evaluation
\end{IEEEkeywords}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
\IEEEpeerreviewmaketitle

\section{Introduction}
Image stitching is the process of generating a natural panoramic image composite from a given set of images. Recently this specialized image mosaicing method has become increasingly common. Stitched images are used in applications like 360-degree photo spheres, architectural walk-throughs, multi-node movies and other applications involving 3D modeling of environments using images of the real world.

The process of stitching involves feature extraction, inlier detection and finding a seam/overlapping region for stitching. After the stitching, further processing like color correction and blending generates a more visually robust image. Figure \ref{Figure 1} shows this activity flow.

In the image acquisition step, each image will cover a Field of View (FoV) which can be obtained by the same Point of Vision (PoV) or multiple points of vision. This distinction is important in image stitching since it will affect the required correction. Stitching images taken from a single PoV is called 1D and when there are multiple PoVs its called 2D. \cite{Khan2012}
Our work focuses on 1D images for the training and evaluation.

\begin{figure}[!t]
\centering
\includegraphics[width=3.5in]{img1_2.eps}
\caption{Flow of an Image Stitching Algorithm}
\label{Figure 1}
\end{figure}

\section{Methodology}
In a review of the related work it became clear that  many image stitching approaches have been suggested with varying focus on performance and the stitching quality. These methods vary in their choice of features to identify key points like Harris Corner detection, SIFT or SURF. They also vary in the method used for generating the overlapping area (for example, generating homographies with methods like RANSAC), the method used to find the optimal seam as well as the post processing techniques.

Getting a quantitative value for the quality of the stitching is a challenge which has been thoroughly researched. Estimating a value for the structural quality of the stitched image \cite{Khan2012} and photometric quality of the image \cite{Khan2012} has been attempted in many settings. Metrics such as SSIM, MSSIM \cite{Wang2004} have been used for representing the structural quality of a stitched image while SAM \cite{Wald2002} and intensity change rate have been used to represent the photometric quality.

The objective of the research work presented in this paper is to find the most robust image stitching algorithm. In doing this many currently available evaluation frameworks have been implemented and tested.

Firstly, we present four image stitching algorithms, and the basic ideas on which they work. Secondly, we introduce four objective evaluation criteria for image quality assessment and discuss merits and shortcomings in each of them. Thirdly, we present the results; a summary of various assessments on a set of images representing various characteristics. Finally, we provide a conclusion of our findings and convey their importance for future work in the field of image stitching; specifically panoramic image stitching.

\section{Stitching Algorithms}
\subsection{Stitching with Invariant Features}

The Brown and Lowe Method (BLM) \cite{Lowe2004} for stitching is one of the most widely recognized methods for image stitching, which is implemented in the OpenCV image pipeline which was used for testing along with other algorithms which were implemented.

In their paper, the author presents a method for extracting invariant features, in order to map similar portions in two images. These features are called invariant features, as they remain robust to differences of scale, rotation, noise addition, change in 3D viewpoint or illumination. After recognizing the features, the features are mapped against a database of features extracted from known objects using a nearest neighbour approach. This is followed by a Hough transform and a least squares approach for consistent pose parameters.

This algorithm is successful in stitching images with clutter and occlusion \cite{Lowe2004} 

\subsection{Fast Panorama Stitching}

Xiong et. al. \cite{Xiong2010} presents a fast and memory efficient method for image stitching, targeted for mobile phones. Given a set of images, it performs colour correction, optimal seam finding through dynamic programming and blending, while each image is stitched with the existing panoramic image which ensures a sequential procedure, thus linear memory requirement.

This requires the identification of the overlapping region between adjacent images for which the methods presented in \cite{Adams2008} will be used. The alignment is also calculated with minimal processing, so that it can run on mobile devices with low resources. A digest of the image is created with the integral projections of the differentials along the x, y, u and v axes. (u, v are the major diagonals). Later, these digests for the two images are aligned in such a way that the differences are minimal. Figure \ref{Figure 2} - \ref{Figure 4} illustrates the methodology described.

\begin{figure}[!t]
\centering
\includegraphics[width=1.9in]{1.eps}
\caption{Left Image and its Integral Projection Graph}
\label{Figure 2}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=1.9in]{2.eps}
\caption{Right Image and its Integral Projection Graph}
\label{Figure 3}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=3.5in]{3.eps}
\caption{Minimal Difference Overlap for the two Integral Projection Graphs}
\label{Figure 4}
\end{figure}

Colour and luminance compensation is done by calculating the local colour correction coefficients which are the ratios between linearized gamma-corrected RGB values for the overlapping area between adjacent images. The local coefficients are then used to calculate a global adjustment factor for the entire panorama. 
 
Given the adjacent images $S_{i-1}$ and $S_{i}$ and their overlapping region is $S_{o,i-1}$ and $S_{o,i}$ the local colour correction coefficient for $S_{i}$  is computed as follows.

\begin{align}
\label{eqn:eq1}
\begin{split}
$\alpha_{c,i} = \frac{\sum_{p}(P_{c,i-1}(P))^\gamma}{\sum_{p}(P_{c,i}(P))^\gamma}$ \\
$c \in \{R,G,B\}(i=1,2,3,...,N)$ .
\end{split}
\end{align}

Here $\gamma$ is the gamma coefficient which is set to 2.2. (This can be tuned for the specific image set to yield better results). $P_{c,i}(p)$ is the colour value pixel $p$ of image $S_{o,i}$. For the first image $a_{c,i} = 1$.

Then using the values obtained a global coefficient is calculated for all channels such that $g_{c}*a_{c,i}$ approximates to 1.

\begin{align}
\label{eqn:eq2}
\begin{split}
$g_{c} = \frac{\sum_{i=0}^{n}\alpha_{c,i}}{\sum_{i=0}^{n}\alpha^{2}_{c,i}}$ \\
$c \in \{R,G,B\}(i=1,2,3,...,N)$ .
\end{split}
\end{align}

To find the optimal seam for stitching, the error surface is calculated for the overlapping region between adjacent images and then the cumulative minimum squared difference for the error surface is computed and the optimal seam is identified by using dynamic programming. When calculating the error surface we tested for both difference of pixel intensity and sum of differences of colour intensities for individual channels and found out that the latter yielded better results.

\subsection{Image Stitching with Harris Edges}

Yuan et. al. \cite{Zheng2008} proposes a dynamic foreground extraction method to be used with video stitching. The proposed method use Harris Corner Detection \cite{Harris1988} and RANSAC \cite{Lacey2000} for stitching the extracted backgrounds.
The proposed method does not give finer details of the implementation therefore the functions available for Harris and RANSAC in OpenCV library were used to implement the algorithm.

\subsection{Image Stitching with Non-Linear Blending}

An efficient video stitching algorithm is explored in \cite{Zheng2008} which also incorporates a method for Non-Linear Blending (NLB) which aims to remove many of the visual artifacts generated in the stitching process (such as ghosting). The key idea in this method is to reuse an initially computed homography mapping between the original frames of the video. Assuming the cameras are stationary, this allows the most computation-intensive part of the process to be pre-computed. SIFT \cite{Lowe2004} is used as a feature and RANSAC \cite{Forsyth2003} is used for inlier detection. The proposed method was implemented using the OpenCV implementation of SIFT and using RANSAC, along with a custom implementation of the proposed non-linear blending method.

\section{Evaluation Frameworks}
\subsection{Subjective Evaluation}
A subjective evaluation scheme where a set of stitched images were ranked according to the quality of stitching by human inspection served as a control for the other evaluation frameworks. This provided an indication of whether the values given by the evaluation metrics give a good representation for the human selection of the best result.

\subsection{A Universal Image Quality Index}
Bovik et al. introduces a universal image quality index \cite{Bovik2002} to compare the quality of two given images. They propose a mechanism of valuing image distortion based on 3 factors : loss of correlation, luminance distortion and contrast distortion.

Let x,y be two comparing images, 
define,

\begin{equation}\label{eq3}
x = \{ x_{i} | i = 1,2,3...N \}
\end{equation}
\begin{equation}\label{eq4}
y = \{ y_{i} | i = 1,2,3...N \}
\end{equation}

\texorpdfstring{x\textsubscript{i}} , \texorpdfstring{y\textsubscript{i}}-  denotes each pixel in the image.

let \makeatletter
\newcommand*{\textoverline{}}[1]{$\overline{\hbox{#x}}\m@th$}
\makeatother
,
\makeatletter
\newcommand*{\textoverline{}}[1]{$\overline{\hbox{#y}}\m@th$}
\makeatother denote the mean values of x and y and, 

$ \sigma_{x} $, $\sigma_{y}$ $\sigma_{xy}$ be standard deviation of x,y and combined standard deviation of x,y. 

\begin{equation}\label{eq5}
Q = \dfrac{4\sigma_{xy} \bar{x} \bar{y}}{( \sigma_{x}^2 + \sigma_{x}^2 )( \bar{x}^2 + \bar{y}^2 )}
\end{equation}

Adapting the concept introduced by the authors, we implemented our stitching evaluation method as follows. Initially, the portion of each original image which contributes to the stitched region (or the seam region) is identified. These portions are extracted from the stitch and the image quality index is calculated individually for each of the three color channels over this region. Based on the human eye’s sensitivity towards colour, green receives a higher weight than red or blue, and this is reflected in our evaluation system by generating a new quality value with the following equation 6. \cite{Poynton1996}

\begin{equation}\label{eq6}
Q = 0.299*R + 0.587*G + 0.114*B
\end{equation}

\subsection{Structural Similarity Comparison as a Evaluation Criteria}

Wang et. al. discusses evaluation of a stitched image in comparison to its original non-altered version by a combined metric of luminance, contrast and structure. \cite{Wang2004} The developed method is called the SSIM (Structural SIMilarity) Index. The index is developed in such a way that it satisfies the conditions of symmetry, boundedness and having a unique maximum. 

The luminance measurement is taken to be qualitatively consistent to Weber’s law, which states that in the human visual system, the noticeable change in luminance is approximately proportional to the background luminance.

Likewise, the contrast measurement is also consistent with the human visual system, by noticing only the relative change in contrast, as opposed to the absolute contrast difference.

The final SSIM index is a product of the above two values, together with a structural similarity component, which is calculated also based on luminance and contrast measurements.

\subsection{Geometric and Photometric Quality of Stitching}

Qureshi et. al. \cite{Khan2012} focuses on giving a quantitative value for the quality of stitching in a stitched panoramic image. The paper proposes two metrics, one to evaluate the geometric quality of the stitching and another to evaluate the photometric quality of the image

\subsubsection{Geometric Quality Assessment}
Geometric quality represents good geometric alignment and seamless flow in the structure of the stitched image.
The two original un-stitched images and the final result of the stitching is used in the evaluation. The method uses the SSIM \cite{Bovik2002} \cite{Wang2004} index and A-trous wavelet filter \cite{Starck1994} to evaluate structural similarity between blocks of 32 pixels and using this value,it will get a geometric quality index for the stitching. 
When implementing the proposed method a bilateral filter was used to generate the low pass component of the images and the high pass component was obtained by subtracting the low pass component from the original image.

\subsubsection{Photometric Quality Assessment}

The photometric quality represents the color and intensity similarity and seamlessness in the stitched image.
Two indexes for color and intensity have been proposed by the authors. 
The color quality assessment index uses the SAM \cite{Wald2002} index to find the angle between the adjacent pixel color vectors and use that as an indicator of the color quality of the stitching.

\begin{equation}\label{eq7}
SAM = arccos \biggl( \dfrac{<p1,p2>}{|p1||p2|} \biggr)
\end{equation}

The IMR value is used to get a value for the intensity gradient between adjacent pixels and that is used as an indicator of the intensity quality of the stitching.

\begin{equation}\label{eq8}
IMR = \dfrac{min(|p1|,|p2|}{max(|p1|,|p2|)}
\end{equation}

Here, p1 and p2 correspond to adjacent pixel vectors.

\section{Evaluation and Results}
\subsection{Test Configuration}
The set of 45 images used for this was created by the authors of the paper, this includes various compositions of images to give a higher range in testing. To improve range of test images, we selected few parameters such as sensor types, light conditions of the scene (indoor,outdoor), scenarios with shallow depth of field and larger field depth. To have variety of capturing sensors, we have chosen images captured from cameras with specs shown in table \ref{tbl1}.

\begin{table}[h]
\centering
\begin{tabular}{ |p{0.7in}|p{0.7in}|p{0.6in}|p{0.7in}| } 
 \hline
 Model & Sensor Size & Image Size & Lens \\ 
 \hline
 GoPro Hero & 5.75 mm × 4.28 mm sensor & 5MP & 10mm f2.8 \\ 
 \hline
 Canon 600D & 22.3mm  x 14.9mm CMOS sensor & 18MP & 18-55mm f3.5-f5.6 \\
 \hline
 Nexus 6 Mobile camera & 4.45mm x 3.52mm CMOS sensor & 13 MP & 28 mm f/2.0 \\
 \hline
\end{tabular}
\\
\caption{Capture Devices}
\label{tbl1}
\end{table}

To have normalized image quality, all captures done under auto mode of each camera.

These images were stitched using the algorithms described in section III. 

For each stitching process, the processing time was computed on a machine with following specifications. 

Intel ® Core i7-4500U processor with four 1.80GHz CPUs, with a 7.7 GiB of RAM on a 64-bit unix Operating System.

\subsection{Evaluation}
Table \ref{tbl2} contains the results from the subjective evaluation. Subjects were asked to rank the stitched images from 1 (best) to 4 (worst). The average response by subjects over the image-set, rounded to the second decimal is taken as the index.


\begin{table}[h]
\centering
\begin{tabular}{ |c|c| } 
 \hline
Stitching Algorithm & Index \\ 
 \hline
BLM & 1.04 \\
Harris & 4.00 \\
NLB & 2.26 \\
Fast & 2.70 \\
 \hline
\end{tabular}
\\
\caption{Subjective Evaluation}
\label{tbl2}
\end{table}

Resulting stitched images are sent to the evaluation algorithms described in section IV, with the two original images. The MSSIM index is a representation of the structural similarity of the overall image, with each of the original images. As a quality evaluation metric, the minimum value of the two is taken as the final MSSIM measurement.

Geometric quality index is also a structural measurement, which takes into account abnormalities which are contrastingly apparent to the Human Visual System.

Photometric quality index evaluates the color correction level of the images. This metric compares the overall image for the similarity in color, and as a result, this captures a correctness value for the images, which are not apparent to the Human Visual System directly.

The UQI measurement is representative of the distortion of luminance, contrast and correlation between pixels.

\begin{table*}[t]
  \centering
  \begin{tabular}{ |p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}|p{0.4in}| } 
 \hline
 \textbf{} & \multicolumn{2}{|c|}{\textbf{MSSIM}} & \multicolumn{2}{|p{0.4in}|}{\textbf{Geometric}}  &  \multicolumn{2}{|p{0.4in}|}{\textbf{Photometric Color}} & \multicolumn{2}{|p{0.4in}|}{\textbf{Photometric Intensity}} &  \multicolumn{2}{|p{0.4in}|}{\textbf{UQI}} \\ 
 \hline
 &Mean&Std. Dev&Mean&Std. Dev&Mean&Std. Dev&Mean&Std. Dev&Mean&Std. Dev \\ 
 \hline
\textbf{BLM} & 0.01475 & 0.03310 & 0.70950 & 0.11480 & 0.92169 & 0.12480 & 1.00000 & 0.00000 & 0.61129 & 0.23581 \\
\hline
\textbf{Harris} & 0.06849 & 0.13411 & 0.13455 & 0.21090 & 0.07034 & 0.16757 & 0.23663 & 0.34092 & 0.13244 & 0.16631 \\
\hline
\textbf{NLB} & 0.15939 & 0.23043 & 0.88295 & 0.11609 & 0.89685 & 0.16281 & 0.98692 & 0.08576 & 0.79860 & 0.22217 \\
\hline
\textbf{Fast} & -0.00482 & 0.02451 & 0.71014 & 0.12436 & 0.96783 & 0.06244 & 0.99962 & 0.00163 & 0.16029 & 0.31478 \\
 \hline
\end{tabular}
\caption{Evaluation Results Summary}
\label{tbl3}
\end{table*}

\begin{figure}[!t]
\centering
\includegraphics[width=3.5in]{new2.eps}
\caption{Mean Run Times}
\label{Figure 5}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=3.5in]{new1.eps}
\caption{Evaluation Results}
\label{Figure 6}
\end{figure}

\subsection{Observations}
The following observations stood out from among the evaluation results as shown in the graphs in figures \ref{Figure 5} and \ref{Figure 6}, and Table \ref{tbl3} .

The “Fast Stitching” algorithm always results in a stitched image, but with varying success. The correctness of the stitched product heavily depends on the image alignment component. Hence, the algorithm’s results are at its best when the overlap between original images is maximal. The algorithm, with its colour correction component, gives visually accurate results, but these results score a lower preference value by human observers in comparison to other stitching algorithms.

The time complexity of the stitching algorithm depends heavily on the image size, and as the image size increases, the time-cost increases rapidly.

In contrast, the Harris stitcher appears to be inept at stitching complex images. One observation that can be derived from the results for this stitcher is that, the feature extraction process is not optimal for panoramic image stitching in real world scenarios. All the evaluation algorithms, as well as the human observers agree that the results from Harris Stitching are suboptimal.

In the Non-linear Blending stitcher the major observation is that it is costly CPU-wise, for example, in DSLR images, it takes significantly longer to produce results. The results from this method are agreeable with the Human Visual System, with occasional ghosting when it comes to images of dynamic situations. It is also observed that the best stitched images contain strong geometric features, such as prominent lines and curves, which lead to more easily matchable SIFT features. The stitching relies heavily on the homography generated using the SIFT feature detection.

The evaluation metrics generally seem to agree that the non-linear blending results are good, with the exception of MSSIM results but even here non-linear blending has the highest result.

Observing the results for Brown and Lowe stitcher, it is shown the time consumption does not depend significantly on the number of pixels, and that for larger images, it gives the best performance. The stitcher is adept at ghost removal, and performs well for all kinds of scenarios.

\section{Conclusion}
The Harris stitching algorithm shows the lowest time overall, this is due to the low complexity of extracting Harris features, however as discussed above it does not provide an acceptable level of accuracy.

Fast stitching algorithm performs faster for smaller images (images with low number of pixels) however the computation time grows with the image size. Thus it is most suitable for panoramic stitching of low quality images. Especially for mobile phones with lower end cameras. But when the picture quality increases this becomes unsuitable.

Even though the Brown and Lowe Method stitches slower for the small images, it is able to perform better than the fast stitching algorithm when the image is larger. This is due to the fact that its complexity is not linear with the pixels rather it depends on the features of the image.

The non linear blending method takes the most amount of time and shows a drastic increase as the image size increase as well. But it can be deemed suitable when the images are well static with an abundance of strong features.

However the time consumed can be improved further through optimizing the algorithms for GPU. 

\appendices
\section{Sample Test Images}

Figures \ref{Figure 7} to \ref{Figure 18} are sample images used for stitching, and the respective stitched results from each method. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Img set 1%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
\includegraphics[width=3.5in]{test1.eps}
\caption{Original Images : Image Set 16 (Captured with Cannon 600D)}
\label{Figure 7}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.2in]{test1_1.eps}
\caption{Stitched with BLM}
\label{Figure 8}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.2in]{test1_2.eps}
\caption{Stitched with Fast Stitching}
\label{Figure 9}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.2in]{test1_3.eps}
\caption{Stitched with NLB}
\label{Figure 10}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
\centering
\includegraphics[width=3.5in]{test2.eps}
\caption{Original Images : Image Set 5 (Captured with Motorola Nexus 6 Mobile camera)}
\label{Figure 11}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test2_1.eps}
\caption{Stitched with BLM}
\label{Figure 12}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test2_2.eps}
\caption{Stitched with Fast Stitching}
\label{Figure 13}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test2_3_.eps}
\caption{Stitched with NLB}
\label{Figure 14}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
\centering
\includegraphics[width=3.5in]{test3.eps}
\caption{Original Images : Image Set 12 (Captured with GoPro HERO)}
\label{Figure 15}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test3_1.eps}
\caption{Stitched with BLM}
\label{Figure 16}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test3_2.eps}
\caption{Stitched with Fast Stitching}
\label{Figure 17}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=2.5in]{test3_3.eps}
\caption{Stitched with NLB}
\label{Figure 18}
\end{figure}

% Can use something like this to put references on a page
% by themselves when using endfloat and the captionsoff option.
\ifCLASSOPTIONcaptionsoff
  \newpage
\fi

%%%%%%%%%%%%%%%%%%%% Bibliography %%%%%%%%%%%%%%
\newpage
\bibliographystyle{unsrt}
\bibliography{main}{}

\end{document}
