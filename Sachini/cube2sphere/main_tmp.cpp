#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/stitching/stitcher.hpp>

using namespace cv;
using namespace std;


Vec3f CubeToRectangle(Vec2f rect_coordinate) {
    Vec3f cube_map_coordinate;
    double PI = atan2(1,1)*4;

    cube_map_coordinate[0] = cos(rect_coordinate[0] * PI * 2) * cos(rect_coordinate[1] * PI);
    cube_map_coordinate[1] = sin(rect_coordinate[1] * PI);
    cube_map_coordinate[2] = sin(rect_coordinate[0] * PI * 2) * cos(rect_coordinate[1] * PI);

    return cube_map_coordinate;
}


int main() {
    cout<<"hi";
    for (int i=0;i<10;i++){
        for (int j=0;j<5;j++){
            Vec2f a = {i/(float)(1000.0), j/(500.0)};
            Vec3f res = CubeToRectangle(a);
            cout<<res[0]<<" "<<res[1]<< " " <<res[2]<<endl;
        }
    }
    return 0;
}
