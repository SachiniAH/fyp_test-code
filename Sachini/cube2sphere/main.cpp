#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;
using namespace std;

int main(){
    Mat image;
    image = imread("colorboxes.jpg", CV_LOAD_IMAGE_COLOR);

    imwrite( "output.jpg", image );
    Mat a = image.clone();
    int w = image.cols;
    int h = image.rows;
    cout<<(w)<< " " << h<<endl;
    for (int i=0;i<w;i++)
        for (int j=0;j<h;j++){
            //if (j==0)cout<<i<<endl;
            /*
            // x = 2 i / w - 1
            //y = 2 j / h - 1
            //or y = 1 - 2 j / h depending on the position of pixel 0
            //
            //double x =
            *
    theta = x pi
    phi = y pi / 2
    or phi = asin(y) for spherical vertical distortion
            *
    x = cos(phi) cos(theta)
    y = sin(phi)
    z = cos(phi) sin(theta)
            */

            double x =  (2*i/(double)w - 1);
            double y =  (2*j/(double)h - 1);
            double pi = atan2(1,1)*4.0;
            double theta = x*pi;
            double phi = y*pi/2.0;
            //double phi = asin(y);

            double cube_size = 375; //1500/4 hardcode for now
            x = cos(phi) * cos(theta);
            y = sin(phi);
            double z = cos(phi) * sin(theta);

            //x,y,z denotes ray vector

            //obtain intersection with cube of size 2 (-1,-1,-1) to (1,1,1).
            double sup = max(abs(x),max(abs(y),abs(z)));
            x/= sup;
            y/= sup;
            z/= sup;


            if (x==1){ //mid 3
                x= cube_size/2 + x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 + z* cube_size/2;
                int x_o = cube_size+y;
                int y_o = 2*cube_size+z;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }

            if (x==-1){ //mid 1
                x= cube_size/2 + x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 - z* cube_size/2;
                int x_o = cube_size+y;
                int y_o = z;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }


            if (z==-1){ //mid 2
                x= cube_size/2 + x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 + z* cube_size/2;
                int x_o = cube_size+y;
                int y_o = cube_size+x;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }


            if (z==1){ //mid 4
                x= cube_size/2 - x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 + z* cube_size/2;
                int x_o = cube_size+y;
                int y_o = 3*cube_size+x;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }

            if (y==1){ //bot
                x= cube_size/2 - x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 + z* cube_size/2;
                int x_o = 2*cube_size+x;
                int y_o = 2*cube_size+z;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }

            if (y==-1){ //bot
                x= cube_size/2 + x* cube_size/2;
                y= cube_size/2 + y* cube_size/2;
                z= cube_size/2 + z* cube_size/2;
                int x_o = +x;
                int y_o = 2*cube_size+z;

                //	cout<<x_o<<" , "<<y_o<<" , "<<z<<endl;
                if (x_o < 0 || y_o < 0)cout<<"ERROR!";
                if (x_o > 0 && y_o > 0 && x_o < 1500 && y_o < 1500){
                    Vec3b color = image.at<Vec3b>(Point(y_o,x_o));
                    a.at<Vec3b>(Point(i,j)) = color;
                }
            }
        }

    Size size(1500,750);//the dst image size,e.g.100x100
    Mat dst;//dst image
    resize(a,dst,size);
    imwrite( "back.jpg", dst );
    //waitKey(0);
    return 0;
}