//
// Created by Rajith Vidanaarachchi on 29/6/15.
//

#ifndef FYP_UNIVERSALQUALITYINDEX_H
#define FYP_UNIVERSALQUALITYINDEX_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;


vector<double> universalQualityIndex(cv::Mat original_image, cv::Mat secondary_image);

#endif //FYP_UNIVERSALQUALITYINDEX_H
