//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#ifndef FYP_QUANTITATIVETESTING_H
#define FYP_QUANTITATIVETESTING_H


#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>


std::vector<float> quantitativeTesting(cv::Mat leftImg, cv::Mat rightImg, cv::Mat stitchedImg, float perc);
std::vector<float> quantitativeTestingWithHomography(cv::Mat leftImg, cv::Mat rightImg, cv::Mat stitchedImgL, cv::Mat stitchedImgR);
cv::Mat splitLeftImg(cv::Mat inImg, int percentage);
cv::Mat splitRightImg(cv::Mat inImg, int percentage);
cv::Mat splitStitchedImg(cv::Mat inImg, int percentage, int img1_cols, int img2_cols);

#endif //FYP_QUANTITATIVETESTING_H