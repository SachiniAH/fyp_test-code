#include <cv.h>
#include <highgui.h>
#include <opencv2/stitching/stitcher.hpp>
#include "GraphUtils.h"
#include <cmath>
#include <vector>

#define MAX_IMAGE_COUNT 10

using namespace cv;
using namespace std;

//Mat grImages[MAX_IMAGE_COUNT];

int temp;
int counter = 0;

char * output_name;


vector<Point2f> detectCorners(Mat &color, int k, int r=4){
	RNG rng(12345);
	vector<Point2f> corners;
	double qualityLevel = 0.01;
	double minDistance = 10;
	int blockSize = 3;
	bool useHarrisDetector = false;
	double K = 0.04;
	int maxCorners = k;

	Mat copy = color.clone();

	Mat grayscale;
	cvtColor( color, grayscale, CV_RGB2GRAY );

	goodFeaturesToTrack( grayscale, corners, maxCorners, qualityLevel,
						minDistance, Mat(), blockSize, useHarrisDetector, K);

	
  	for( int i = 0; i < corners.size(); i++ ){
		circle( copy, corners[i], r, Scalar(rng.uniform(0,255), rng.uniform(0,255),
              rng.uniform(0,255)), -1, 8, 0 ); 
	}

	//imshow( "Alignment", copy);
	//waitKey(0);

	return corners;

}

vector<int>  calculateIntegrals2(Mat &gray_1, Mat &kernal, int _case){

	Point anchor = Point (-1,1);
	double delta = 0;
	int ddepth = -1;

	Mat temp;
	Mat gray;

	// Apply the differential filter
	filter2D(gray_1, temp, ddepth, kernal, anchor, delta, BORDER_DEFAULT);

	//imshow( "Alignment", temp);
	//waitKey(0);

	// Raise to the square
	pow(temp, 2, gray);

	//imshow( "Alignment", gray);
	//waitKey(0);

	

	// Calculating integral projections
	vector<int> integral;

	// _case 1 : Horizontal Differentiation : Vertical Integration
	if(_case == 1){
		integral.clear();
		for (int i = 0; i < gray.cols; i++) {
			int temp_sum = 0;
		    for (int j = 0; j < gray.rows; j++) {
		        temp_sum += (int)gray.at<uchar>(j, i);
		    }
			integral.push_back(temp_sum);		
		}
	}

	// _case 2 : Vertical Differentiation : Horizontal Integration
	else if(_case == 2){
		integral.clear();
		for (int i = 0; i < gray.rows; i++) {
			int temp_sum = 0;
		    for (int j = 0; j < gray.cols; j++) {
		        temp_sum += (int)gray.at<uchar>(i, j);
		    }
			integral.push_back(temp_sum);	
		}
	}

	// _case 3 : Differentiation along -ve Diagonal : Integration along +ve Diagonal
	else if(_case == 3){
		integral.clear();
		for (int i=0; i < gray.rows; i++){
			int j = 0;
			int temp_sum = 0;
			int temp_no = 1;
			for(int k=0; i-k > 0 && j+k < gray.cols; k++){
				temp_sum += (int)gray.at<uchar>(i-k, j+k);
				temp_no ++;
			}
			integral.push_back(temp_sum / temp_no);
		}

		for (int j=1; j < gray.cols; j++){
			int i = gray.rows-1;
			int temp_sum = 0;
			int temp_no = 1;
			for(int k=0; i-k > 0 && j+k < gray.cols; k++){
				temp_sum += (int)gray.at<uchar>(i-k, j+k);
				temp_no ++;
			}
			integral.push_back(temp_sum / temp_no);
		}
	}

	// _case 4 : Differentiation along +ve Diagonal : Integration along -ve Diagonal
	else if(_case == 4){	
		integral.clear();
		for (int i=0; i < gray.rows; i++){
			int j = 0;
			int temp_sum = 0;
			int temp_no = 1;
			for(int k=0; i+k < gray.rows && j+k < gray.cols; k++){
				temp_sum += (int)gray.at<uchar>(i+k, j+k);
				temp_no ++;
			}
			integral.push_back(temp_sum/temp_no);
		}

		for (int j=1; j < gray.cols; j++){
			int i = 0;
			int temp_sum = 0;
			int temp_no = 1;
			for(int k=0; i+k < gray.rows && j+k < gray.cols; k++){
				temp_sum += (int)gray.at<uchar>(i+k, j+k);
				temp_no ++;
			}
			integral.push_back(temp_sum / temp_no);
		}
	}

	return integral;
	
}

void printIntegral(string title, vector<int> integral){
	cout << title << endl;
	for(int i=0; i<integral.size(); i++){
		cout << integral[i] << "\t";
	}
	cout << endl;
}

vector<vector<int>> calculateIntegrals(Mat &color){
	Mat temp;
	Mat gray;
	vector<vector<int>> result;

	// Convert the picture into Grayscale
	cvtColor (color, gray, CV_RGB2GRAY);
	

	Mat hor_kernal = (Mat_<double>(3,3) << 1, 0, -1, 2, 0, -2, 1, 0, -1);
	result.push_back(calculateIntegrals2(gray, hor_kernal, 1));
	
	Mat ver_kernal = (Mat_<double>(3,3) << 1, 2, 1, 0, 0, 0, -1, -2, -1);
	result.push_back(calculateIntegrals2(gray, ver_kernal, 2));

	Mat dia_kernal = (Mat_<double>(3,3) << 2, 1, 0, 1, 0, -1, 0, -1, -2);
	result.push_back(calculateIntegrals2(gray, dia_kernal, 3));

	Mat dia2_kernal = (Mat_<double>(3,3) << 0, 1, 2, -1, 0, 1, -2, -1, 0);
	result.push_back(calculateIntegrals2(gray, dia2_kernal, 4));


	counter++;
	return result;
}

int findMinDist( vector<int> vec1, vector<int> vec2 ){
	float dif = 1000000000;
	int pos = 0;

	for(int i=0; i<vec1.size()-6; i++){
		float temp1 = 0;
		float temp2 = 0;
		for(int j=5; i+j<vec1.size()-5; j++){
			temp1 += (vec1[j] - vec2[i+j])*(vec1[j] - vec2[i+j]);
			temp2 += (vec1[i+j] - vec2[j])*(vec1[i+j] - vec2[j]);
		}

		temp1 = temp1/((vec1.size()-i)*1.0);
		temp2 = temp2/((vec1.size()-i)*1.0);

		//cout << i << " " << temp1 << " " << temp2 << endl

		if(temp1 < dif && temp1 > 100000 && i < vec1.size()/2){
			dif = temp1;
			pos = -i;
		}

		if(temp2 < dif && temp2 > 100000 && i < vec1.size()/2){
			dif = temp2;
			pos = i;
		}
	}
	return pos;
}

// For the Horizontal alignment. Tweaked for horizontal alignment because it plays the major role in overall alignment.
int findMinDist2(vector<int> vec1, vector<int> vec2){
	float dif = 1000000000;
	int pos = 0;

	for(int i=0; i<vec1.size()-6; i++){
		float temp1 = 0;
		float temp2 = 0;
		for(int j=5; i+j<vec1.size()-5; j++){
			temp1 += (vec1[j] - vec2[i+j])*(vec1[j] - vec2[i+j]);
			temp2 += (vec1[i+j] - vec2[j])*(vec1[i+j] - vec2[j]);
		}

		temp1 = temp1/((vec1.size()-i)*1.0);
		temp2 = temp2/((vec1.size()-i)*1.0);

		//cout << i << " " << temp1 << " " << temp2 << endl

		if(temp2 < dif && temp2 > 100000 && i < vec1.size()/4*3){
			dif = temp2;
			pos = i;
		}
	}
	return pos;
}

void printDifGraphs( vector<int> vec1, vector<int> vec2, int diff, char* name) {
	vector<int> plot;
	plot.clear();
	int dif = (int)abs((float)diff);
	if(diff <= 0){
		for(int i=0; i<dif; i++){
			plot.push_back(vec2[i]);
			//cout << "#" << i << endl;	
		}
		for(int i=0; i<vec2.size()-dif; i++){
			plot.push_back((int)abs((float)(vec1[i]-vec2[dif+i])));
			//cout << "%" << dif+i << endl;
		}
		for(int i=vec1.size()-1-dif; i<vec1.size(); i++){
			//cout << "*" << i << endl;
			plot.push_back(vec1[i]);
		}
	}else{
		for(int i=0; i<dif; i++){
			plot.push_back(vec1[i]);	
		}
		for(int i=0; i<vec1.size()-dif; i++){
			plot.push_back((int)abs((float)(vec2[i]-vec1[dif+i])));
		}
		for(int i=vec2.size()-1-dif; i<vec2.size(); i++){
			plot.push_back(vec2[i]);
		}
	}


	//IplImage *graphImg = drawIntGraph(&vec1[0], vec1.size(), NULL,
	//-255,255, 400,180, "Graph" );
	//drawIntGraph(&vec2[0], vec2.size(), graphImg, -255,255, 400,180);
	//drawIntGraph(&plot[0], plot.size(), graphImg, -255,255, 400,180);



	// UNCOMMENT HERE TO PRINT THE INTEGRAL PROJECTION GRAPHS

	showIntGraph("one", &vec1[0], vec1.size());

	showIntGraph("two", &vec2[0], vec2.size());
	showIntGraph("diff", &plot[0], plot.size());

	//showImage(graphImg, 0, "Graph");
	//waitKey(0);

}

Mat overlapImage(Mat m1, Mat m2, int diff){
	Mat m3(m1.rows, m2.cols + m1.cols + diff, 16);
	for(int q=0; q<m1.rows; q++){
		int dif = (int)abs((float)diff);
		int temp = 0;
		if(diff <= 0){
			for(int i=0; i<dif; i++){
				//plot.push_back(vec2[i]);
				m3.at<Vec3b>(q, ++temp) = m2.at<Vec3b>(q, i);
				//cout << "#" << i << endl;	
			}
			for(int i=0; i<m2.cols-dif; i++){
				//plot.push_back((int)abs((float)(vec1[i]-vec2[dif+i])));
				m3.at<Vec3b>(q, temp+i)[0] = (m1.at<Vec3b>(q, i)[0] + m2.at<Vec3b> (q, dif+i)[0])/2;
				m3.at<Vec3b>(q, temp+i)[1] = (m1.at<Vec3b>(q, i)[1] + m2.at<Vec3b> (q, dif+i)[1])/2;
				m3.at<Vec3b>(q, temp+i)[2] = (m1.at<Vec3b>(q, i)[2] + m2.at<Vec3b> (q, dif+i)[2])/2;
				//cout << "%" << dif+i << endl;
			}
			temp += m2.cols-dif;
			for(int i=m1.cols-1-dif; i<m1.cols; i++){
				//cout << "*" << i << endl;
				m3.at<Vec3b>(q, ++temp) = m1.at<Vec3b>(q, i);
				//plot.push_back(vec1[i]);
			}
		}else{
			for(int i=0; i<dif; i++){
				//plot.push_back(vec1[i]);
				m3.at<Vec3b>(q, ++temp) = m1.at<Vec3b>(q, i);
			}
			for(int i=0; i<m1.cols-dif; i++){
				//plot.push_back((int)abs((float)(vec2[i]-vec1[dif+i])));
				m3.at<Vec3b>(q, temp+i)[0] = (m2.at<Vec3b>(q, i)[0] + m1.at<Vec3b> (q, dif+i)[0])/2;
				m3.at<Vec3b>(q, temp+i)[1] = (m2.at<Vec3b>(q, i)[1] + m1.at<Vec3b> (q, dif+i)[1])/2;
				m3.at<Vec3b>(q, temp+i)[2] = (m2.at<Vec3b>(q, i)[2] + m1.at<Vec3b> (q, dif+i)[2])/2;
			}
			temp += m1.cols-dif;
			for(int i=m2.cols-1-dif; i<m2.cols; i++){
				//plot.push_back(vec2[i]);
				m3.at<Vec3b>(q, ++temp) = m2.at<Vec3b>(q, i);
			}
		}
	}

	return m3;
	
}

vector<float> alignment( Mat image1, Mat image2 ){


	vector<vector<int>> img1 = calculateIntegrals(image1);
	vector<vector<int>> img2 = calculateIntegrals(image2);

	int x_dif = findMinDist2(img1[0], img2[0]);
	int y_dif = findMinDist(img1[1], img2[1]);
	int u_dif = findMinDist(img1[2], img2[2]);
	int v_dif = findMinDist(img1[3], img2[3]);

	char * name = "Y Dif";
	//printDifGraphs(img1[2], img2[2], u_dif, name);
	//printDifGraphs(img1[3], img2[3], v_dif, name);

	float t_x = (x_dif*1.0); // + ((u_dif + v_dif)*0.5 / sqrt(2)))/2;
	float t_y = ((-y_dif*1.0) + (  v_dif - u_dif)*0.5 / sqrt(2))/2;

	//cout << endl << "DIFFS" << endl;
	//cout << x_dif << " " << y_dif << " " << u_dif << " " << v_dif << endl << endl;

    float percentage_x = ((t_x * 100 )/ image1.cols);
	float percentage_y = ((t_y * 100 )/ image1.rows);

	if(percentage_x < 0){
		percentage_x = - ( 100 + percentage_x);
	}
	else{
		percentage_x = 100 - percentage_x;
	}

	if(percentage_y < 0){
		percentage_y = - (100 + percentage_y);
	}
	else{
		percentage_y = 100 -percentage_y;
	}

    //cout << percentage_x << " " << percentage_y << endl;

    vector<float> return_val;

	return_val.push_back(percentage_x);
	return_val.push_back(100.0f);

	//vector<Point2f> img1_corners = detectCorners(images[0], 32);
	//vector<Point2f> img2_corners = detectCorners(images[1], 32);

	return return_val;
}
