//
// Created by Sachith Seneviratne on 6/6/15.
//
#include <stdio.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

Mat findHomography(Mat image1, Mat image2)
{

    Mat img_object;
    Mat img_scene;
    Mat img_object1 = image1;
    Mat img_scene1 = image2;

    cvtColor(image1, img_object, CV_RGB2GRAY);
    cvtColor(image2, img_scene, CV_RGB2GRAY);


    //-- Step 1: Detect the keypoints using SIFT Detector
    int minHessian = 400;

    SiftFeatureDetector detector(minHessian);

    std::vector<KeyPoint> keypoints_object, keypoints_scene;

    detector.detect(img_object, keypoints_object);
    detector.detect(img_scene, keypoints_scene);

    //-- Step 2: Calculate descriptors (feature vectors)
    SiftDescriptorExtractor extractor;

    Mat descriptors_object, descriptors_scene;

    extractor.compute(img_object, keypoints_object, descriptors_object);
    extractor.compute(img_scene, keypoints_scene, descriptors_scene);

    //-- Step 3: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match(descriptors_object, descriptors_scene, matches);

    double max_dist = 0; double min_dist = 100;
    vector <double> dists;
    //-- Quick calculation of max and min distances between keypoints
    for (int i = 0; i < descriptors_object.rows; i++)
    {
        double dist = matches[i].distance;
        dists.push_back(matches[i].distance);
        if (dist < min_dist) min_dist = dist;
        if (dist > max_dist) max_dist = dist;
    }

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;

    for (int i = 0; i < descriptors_object.rows; i++)
    {
        if (matches[i].distance < 3                                                                                                                                                                                              * min_dist || true)
        {
            good_matches.push_back(matches[i]);
        }
    }

    Mat img_matches;
    drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
                good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    //-- Localize the object
    std::vector<Point2f> obj;
    std::vector<Point2f> scene;

    for (int i = 0; i < good_matches.size(); i++)
    {
        //-- Get the keypoints from the good matches
        obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
        scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
    }

    //Mat H = findHomography(scene, obj, CV_RANSAC,2);
    Mat H = findHomography(scene, obj, CV_RANSAC);

    return H;
}


vector<Mat> getOverlapSingle(Mat img_object1, Mat img_stitched){
    vector<Mat> ans;

    Mat H = findHomography(img_object1, img_stitched);

    Mat C;
    //perspectiveTransform(img_object1, C, H);
    warpPerspective(img_object1, C, H, cv::Size(img_stitched.cols, img_stitched.rows), WARP_INVERSE_MAP);



    for (int y = 0; y<C.rows; y++)
    {
        for (int x = 0; x<C.cols; x++)
        {
            if (C.at<Vec3b>(Point(x, y))[0] == 0 && C.at<Vec3b>(Point(x, y))[1] == 0 && C.at<Vec3b>(Point(x, y))[2] == 0)
            if (C.at<Vec3b>(Point(x, y)).dot({ 1, 1, 1 }) == 0)img_stitched.at<Vec3b>(Point(x, y)) = { 0, 0, 0 };

            if (img_stitched.at<Vec3b>(Point(x, y))[0] == 0 && img_stitched.at<Vec3b>(Point(x, y))[1] == 0 && img_stitched.at<Vec3b>(Point(x, y))[2] == 0)
            if (img_stitched.at<Vec3b>(Point(x, y)).dot({ 1, 1, 1 }) == 0)C.at<Vec3b>(Point(x, y)) = { 0, 0, 0 };
        }
    }




    ans.push_back(C);
    ans.push_back(img_stitched);


    return ans;
}

vector<Mat> getOverlapImages(Mat img_object1, Mat img_scene1, Mat img_stitched){
    vector<Mat> ans1, ans2;
    Mat img_stitched1 = img_stitched.clone();
    Mat img_stitched2 = img_stitched.clone();

    ans1 = getOverlapSingle(img_object1, img_stitched1);
    ans2 = getOverlapSingle(img_scene1, img_stitched2);
    ans1.push_back(ans2[0]);
    ans1.push_back(ans2[1]);

    return ans1;
}