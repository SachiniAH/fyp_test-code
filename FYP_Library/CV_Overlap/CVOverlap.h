//
// Created by Rajith Vidanaarachchi on 13/7/15.
//

#ifndef FYP_CVOVERLAP_H
#define FYP_CVOVERLAP_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;


vector<Mat> getOverlapImages(Mat img_object1, Mat img_scene1, Mat img_stitched);


#endif //FYP_CVOVERLAP_H
