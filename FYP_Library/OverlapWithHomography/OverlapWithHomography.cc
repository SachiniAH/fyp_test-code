//
// Created by Rajith Vidanaarachchi on 27/6/15.
//

#include <iostream>
#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <math.h>

using namespace std;
using namespace cv;

vector<Mat> OverlapAreas(Mat img1, Mat img2, Mat stitched, Mat H){
    //assumes images are stitched horizontally

    Mat overlap1;
    warpPerspective(img1, overlap1, H, cv::Size(img1.cols, img1.rows));
    //std::cout << "1";

    Mat overlap2;
    warpPerspective(img2, overlap2, H, cv::Size(img1.cols, img1.rows), WARP_INVERSE_MAP);
    //std::cout << "1";


    Mat stitchedoverlap1;
    warpPerspective(stitched, stitchedoverlap1, H, cv::Size(img1.cols, img1.rows));
    //std::cout << "1";




    for (int y = 0; y<img1.rows; y++)
    {
        for (int x = 0; x<img1.cols; x++)
        {
            // get pixel
            if (!(overlap1.at<Vec3b>(Point(x, y)).dot(overlap1.at<Vec3b>(Point(x, y))) == 0)){
                //Not black
                stitchedoverlap1.at<Vec3b>(Point(x, y)) = stitched.at<Vec3b>(Point(x, y));
            }
            else
                stitchedoverlap1.at<Vec3b>(Point(x, y)) = Vec3b({ 0, 0, 0 });

            if (!(overlap2.at<Vec3b>(Point(x, y)).dot(overlap2.at<Vec3b>(Point(x, y))) == 0)){
                //Not black
                Point n;
                //   Vector<float> in = { x, y };
                //  perspectiveTransform(in,H);
                // stitchedoverlap2.at<Vec3b>(Point(x, y)) = stitched.at<Vec3b>(Point(x, y));
            }
            else;
            // stitchedoverlap2.at<Vec3b>(Point(x, y)) = Vec3b({ 0, 0, 0 });
        }
    }

    Mat stitchedoverlap2;
    warpPerspective(stitchedoverlap1, stitchedoverlap2, H, cv::Size(img1.cols, img1.rows), WARP_INVERSE_MAP);
    //std::cout << "1";
    Size size(480, 320);//the dst image size,e.g.100x100

    resize(overlap1, overlap1, size);
    resize(stitchedoverlap1, stitchedoverlap1, size);
    resize(overlap2, overlap2, size);
    resize(stitchedoverlap2, stitchedoverlap2, size);
    //imwrite("img1", overlap1);
    //imwrite("simg1", stitchedoverlap1);
    //imwrite("img2", overlap2);
    //imwrite("simg2", stitchedoverlap2);

    vector<Mat> results = {overlap1, stitchedoverlap1, overlap2, stitchedoverlap2};

    return results;
}