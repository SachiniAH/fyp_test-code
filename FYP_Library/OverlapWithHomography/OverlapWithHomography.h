//
// Created by Rajith Vidanaarachchi on 27/6/15.
//

#ifndef FYP_OVERLAPWITHHOMOGRAPHY_H
#define FYP_OVERLAPWITHHOMOGRAPHY_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

vector<Mat> OverlapAreas(Mat img1, Mat img2, Mat stitched, Mat H);

#endif //FYP_OVERLAPWITHHOMOGRAPHY_H
