//
// Created by Rajith Vidanaarachchi on 27/6/15.
//

#ifndef FYP_HARRISSTITCHING_H
#define FYP_HARRISSTITCHING_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

vector<Mat> harrisStitching( Mat image1, Mat image2);

#endif //FYP_HARRISSTITCHING_H
