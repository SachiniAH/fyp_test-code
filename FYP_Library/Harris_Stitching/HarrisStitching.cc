//
// Created by Rajith Vidanaarachchi on 27/6/15.
//
#include <stdio.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;


/** @function harrisStitching */
vector<Mat> harrisStitching( Mat image2, Mat image1)
{

// Load the images
    //image1= imread( argv[2] );
    //image2= imread( argv[1] );

    int thresh = 200;
    int max_thresh = 255;

    Mat gray_image1;
    Mat gray_image2;


    gray_image1;
    gray_image2;
    Size size(518,518);//the dst image size,e.g.100x100
//resize(image1,image1,size);//res
//resize(image2,image2,size);

// Convert to Grayscale
    cvtColor( image1, gray_image1, CV_RGB2GRAY );
    cvtColor( image2, gray_image2, CV_RGB2GRAY );

// ****************************** stitch with harris **************************** //

    cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create("HARRIS");
    cv::Ptr<cv::DescriptorExtractor> descriptor = cv::DescriptorExtractor::create("SIFT");


// detect keypoints
    std::vector<cv::KeyPoint> keypoints1, keypoints2;
    detector->detect(gray_image1, keypoints1);
    detector->detect(gray_image2, keypoints2);
    //cout << "detected using harris " << keypoints1.size() << " " << keypoints2.size() << endl;

    //-- Step 2: Calculate descriptors (feature vectors)
    cv::Mat descriptors1, descriptors2;
    SurfDescriptorExtractor extractor;


// using surf descriptor
    extractor.compute( image1, keypoints1, descriptors1 );
    extractor.compute( image2, keypoints2, descriptors2 );
    //cout << "success with surfDescriptor" << endl;

/*
// using sift descriptor
descriptor->compute(gray_image1, keypoints1, descriptors1);
descriptor->compute(gray_image2, keypoints2, descriptors2);
cout << "success with siftDescriptor" << endl;
*/



//-- Step 3: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match( descriptors1, descriptors2, matches );

    double max_dist = 0; double min_dist = 100;

//-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptors1.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

    //printf("-- Max dist : %f \n", max_dist );
    //printf("-- Min dist : %f \n", min_dist );


//-- Use only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;
    int count = 0;
    for( int i = 0; i < descriptors1.rows; i++ )
    { if( matches[i].distance < 7*min_dist )
        {
            good_matches.push_back( matches[i]);
            count ++;
        }
    }

    //cout << "found good matches: " << good_matches.size() << " " << count << endl;
    std::vector< Point2f > selected_points1;
    std::vector< Point2f > selected_points2;

    for( int i = 0; i < good_matches.size(); i++ )
    {
        //-- Get the keypoints from the good matches
        selected_points1.push_back( keypoints1[ good_matches[i].queryIdx ].pt );
        //circle( image1, Point( keypoints1[ good_matches[i].queryIdx ].pt.x, keypoints1[ good_matches[i].queryIdx ].pt.y ), 5,  Scalar( 0, 255, 255 ), 2, 8, 0 );
        selected_points2.push_back( keypoints2[ good_matches[i].trainIdx ].pt );
        //circle( image2, Point( keypoints2[ good_matches[i].queryIdx ].pt.x, keypoints2[ good_matches[i].queryIdx ].pt.y ), 5,  Scalar( 0, 255, 255 ), 2, 8, 0 );
    }

    //imshow("first image",image2);
    //imwrite("/home/sanka/TestImages/harris1.jpg" , image2);
    //imshow("second image",image1);
    //imwrite("/home/sanka/TestImages/harris2.jpg" , image1);

// Find the Homography Matrix
    Mat H = findHomography( selected_points1, selected_points2, CV_RANSAC );

    Mat image_matches;
    drawMatches( image1, keypoints1, image2, keypoints2,
                 good_matches, image_matches, Scalar::all(-1), Scalar::all(-1),
                 vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
    //imwrite("/home/sanka/TestImages/harris_homography.jpg" , image_matches);

    // Use the Homography Matrix to warp the images
    cv::Mat result;
    warpPerspective(image1,result,H,cv::Size(image1.cols+image2.cols,image1.rows));
    cv::Mat half(result,cv::Rect(0,0,image2.cols,image2.rows));
    image2.copyTo(half);
    //imwrite( "/home/sanka/TestImages/harrisResult.jpg" , result);

    //imshow( "Result", result );
// ********************************************************************************* //

    //waitKey(0);

    vector<Mat> results = {result, H};

    return results;
}