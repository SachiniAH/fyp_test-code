//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#include "FYP.h"
#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/stitching/stitcher.hpp>

using namespace std;
using namespace cv;
using namespace std::chrono;

void readme();

void fast_stitching_test(Mat image1, Mat image2, char * output_no){

    high_resolution_clock::time_point t1_AL = high_resolution_clock::now();
    vector<float> shifts = alignment(image1, image2);
    high_resolution_clock::time_point t2_AL = high_resolution_clock::now();

    auto duration_AL = std::chrono::duration_cast<std::chrono::microseconds>( t2_AL - t1_AL ).count();

    float x_shift = shifts[0];
    float y_shift = shifts[1];

    cerr << "Alignment algorithm successfully ran. time : " << duration_AL << "microseconds" << endl;
    cerr << "Shift in the Horizontal Axis " << x_shift << "%" << endl;
    cerr << "Shift in the Vertical Axis" << y_shift << "%" << endl << endl;

    Mat stitched;

    high_resolution_clock::time_point t1_FS = high_resolution_clock::now();
    stitched = fastStitching(image1, image2, shifts);
    high_resolution_clock::time_point t2_FS = high_resolution_clock::now();

    auto duration_Fast = std::chrono::duration_cast<std::chrono::microseconds>( t2_FS - t1_FS ).count();

    cerr << "Stitching complete with Fast Stitching Algorithm" << duration_Fast << "microseconds" << endl;

    string save_name (output_no);
    save_name += "_FastStitching.jpg";
    imwrite(save_name.c_str(), stitched);

    cerr << "Stitched image saved to : " << save_name << endl << endl;


    vector<float> results_IQA = imageQualityAssessment(image1, image2, stitched, x_shift);

    cerr << "Results from Image Quality Assessment :" << endl;
    cerr << "SSIM (left, right) : " << results_IQA[0] << " " << results_IQA[1] << endl;
    cerr << "MSSIM (left, right) : " << results_IQA[2] << " " << results_IQA[3] << endl << endl;

    vector<float> results_Fast_QT = quantitativeTesting(image1, image2, stitched, x_shift);
    cerr << "Results from Quantitative Testing :" << endl;
    cerr << "Geometric Quaity :" << results_Fast_QT[0] << endl;
    cerr << "Photometric Quality : (Color) : " << results_Fast_QT[1] << endl;
    cerr << "Photometric Quality : (Intensity) : " << results_Fast_QT[2] << endl << endl;


    /// Implement Universal Quality Index
    int percentage = (int)round(x_shift);

    Mat leftImg = splitLeftImg(image1, percentage);
    Mat rightImg = splitRightImg(image2, percentage);
    Mat stitchedImg = splitStitchedImg(stitched, percentage, image1.cols, image2.cols);

    vector<double> results_Fast_UQI_L = universalQualityIndex(leftImg, stitchedImg);
    vector<double> results_Fast_UQI_R = universalQualityIndex(rightImg, stitchedImg);

    cerr << "Results from IQA : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "B: " << results_Fast_UQI_L[0] << " G: " << results_Fast_UQI_L[1] << " R: " << results_Fast_UQI_L[2] << endl;
    cerr << "Overall : " << results_Fast_UQI_L[3] << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "B: " << results_Fast_UQI_R[0] << " G: " << results_Fast_UQI_R[1] << " R: " << results_Fast_UQI_R[2] << endl;
    cerr << "Overall : " << results_Fast_UQI_R[3] << endl << endl;

    cout << output_no << ",Fast," << x_shift << "," << results_IQA[0] << "," << results_IQA[1] << "," << results_IQA[2] <<"," <<results_IQA[3] << "," ;
    cout << results_Fast_QT[0] << "," << results_Fast_QT[1] << "," << results_Fast_QT[2] << "," ;
    cout << results_Fast_UQI_L[0] << ","<< results_Fast_UQI_L[1] << ","<< results_Fast_UQI_L[2] << ","<< results_Fast_UQI_L[3] << "," ;
    cout << results_Fast_UQI_R[0] << ","<< results_Fast_UQI_R[1] << ","<< results_Fast_UQI_R[2] << ","<< results_Fast_UQI_R[3] << "," ;
    cout << duration_Fast + duration_AL << endl;

    cerr <<  "Over" << endl;
}

void harris_stitching_test(Mat image1, Mat image2, char * output_no){

    vector <Mat> results_Harris;

    high_resolution_clock::time_point t1_H = high_resolution_clock::now();
    results_Harris = harrisStitching(image1, image2);
    high_resolution_clock::time_point t2_H = high_resolution_clock::now();

    auto duration_Harris = std::chrono::duration_cast<std::chrono::microseconds>( t2_H - t1_H ).count();


    cerr << "Stitched Successfully with Harris Stitching : time : " << duration_Harris << "microseconds" << endl;

    Mat stitched_Harris = results_Harris[0];
    Mat homography_Harris = results_Harris[1];

    string save_name2 (output_no);
    save_name2 += "_HarrisStitching.jpg";
    imwrite(save_name2.c_str(), stitched_Harris);

    cerr << "Stitched Image Saved to : " << save_name2 << endl << endl;

    vector<Mat> overlaps_Harris = OverlapAreas(image2, image1, stitched_Harris, homography_Harris);

/*    string ol_L (output_no);
    ol_L += "_HS_ol_L";
    string ol_LS (output_no);
    ol_LS += "_HS_ol_LS";
    string ol_R (output_no);
    ol_R += "_HS_ol_R";
    string ol_RS (output_no);
    ol_RS += "_HS_ol_RS";*/

    //imshow(ol_L.c_str(), overlaps_Harris[0]);
    //imshow(ol_LS.c_str(), overlaps_Harris[1]);
    //imshow(ol_R.c_str(), overlaps_Harris[2]);
    //imshow(ol_RS.c_str(), overlaps_Harris[3]);



    cerr << "Overlap Areas Found " << overlaps_Harris.size() << endl;

    vector<float> results_Harris_QT = quantitativeTestingWithHomography(overlaps_Harris[0], overlaps_Harris[2], overlaps_Harris[1], overlaps_Harris[3]);

    cerr << "Results from Quantitative Testing :" << endl;
    cerr << "Geometric Quaity :" << results_Harris_QT[0] << endl;
    cerr << "Photometric Quality : (Color) : " << results_Harris_QT[1] << endl;
    cerr << "Photometric Quality : (Intensity) : " << results_Harris_QT[2] << endl << endl;

    //cout << results_Harris_QT[0] << " " << results_Harris_QT[1] << " " << results_Harris_QT[2] << endl;

    vector<double> results_Harris_UQI_L = universalQualityIndex(overlaps_Harris[0], overlaps_Harris[1]);
    vector<double> results_Harris_UQI_R = universalQualityIndex(overlaps_Harris[2], overlaps_Harris[3]);

    cerr << "Results from Unviersal Quality Index : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "B: " << results_Harris_UQI_L[0] << " G: " << results_Harris_UQI_L[1] << " R: " << results_Harris_UQI_L[2] << endl;
    cerr << "Overall : " << results_Harris_UQI_L[3] << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "B: " << results_Harris_UQI_R[0] << " G: " << results_Harris_UQI_R[1] << " R: " << results_Harris_UQI_R[2] << endl;
    cerr << "Overall : " << results_Harris_UQI_R[3] << endl << endl;

    // Implement IQA

    vector<float> results_Harris_IQA_L = imageQualityAssessmentHomography(overlaps_Harris[0], overlaps_Harris[1]);
    vector<float> results_Harris_IQA_R = imageQualityAssessmentHomography(overlaps_Harris[2], overlaps_Harris[3]);

    cerr << "Results from IQA : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "SSIM: " << results_Harris_IQA_L[0] << " MSSIM: " << results_Harris_IQA_L[1] << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "SSIM: " << results_Harris_IQA_R[0] << " MSSIM: " << results_Harris_IQA_R[1] << endl;

    cout << output_no << ",Harris,," << results_Harris_IQA_L[0] << "," << results_Harris_IQA_R[0] << "," << results_Harris_IQA_L[1] <<"," <<results_Harris_IQA_R[1] << "," ;
    cout << results_Harris_QT[0] << "," << results_Harris_QT[1] << "," << results_Harris_QT[2] << "," ;
    cout << results_Harris_UQI_L[0] << ","<< results_Harris_UQI_L[1] << ","<< results_Harris_UQI_L[2] << ","<< results_Harris_UQI_L[3] << "," ;
    cout << results_Harris_UQI_R[0] << ","<< results_Harris_UQI_R[1] << ","<< results_Harris_UQI_R[2] << ","<< results_Harris_UQI_R[3] << "," ;
    cout << duration_Harris << endl;

    cerr <<  "Over" << endl;

}

void opencv_stitching_test(Mat image1, Mat image2, char * output_no){

    cerr << "Open CV stitching" << endl;

    Mat result;
    Stitcher stitcher = Stitcher::createDefault();
    vector<Mat> input = {image1, image2};

    Mat stitched_CV;

    high_resolution_clock::time_point t1_CV = high_resolution_clock::now();
    Stitcher::Status status = stitcher.stitch(input, stitched_CV);
    high_resolution_clock::time_point t2_CV = high_resolution_clock::now();

    auto duration_CV = std::chrono::duration_cast<std::chrono::microseconds>( t2_CV - t1_CV ).count();


    cerr << "Stitched Successfully with OpenCV Stitching : time : " << duration_CV << "microseconds" << endl;

    string save_name2 (output_no);
    save_name2 += "_CVStitching.jpg";
    imwrite(save_name2.c_str(), stitched_CV);

    cerr << "Stitched Image Saved to : " << save_name2 << endl << endl;


    vector<Mat> overlaps_CV = getOverlapImages(image2, image1, stitched_CV);

    /*Size size(500, 500);

    Mat op_ol_L;
    Mat op_ol_LS;
    Mat op_ol_R;
    Mat op_ol_RS;

    resize(overlaps_CV[0], op_ol_L, size);
    resize(overlaps_CV[1], op_ol_LS, size);
    resize(overlaps_CV[2], op_ol_R, size);
    resize(overlaps_CV[3], op_ol_RS, size);

    string ol_L (output_no);
    ol_L += "_CV_ol_L";
    string ol_LS (output_no);
    ol_LS += "_CV_ol_LS";
    string ol_R (output_no);
    ol_R += "_CV_ol_R";
    string ol_RS (output_no);
    ol_RS += "_CV_ol_RS";*/

    //imshow(ol_L.c_str(), op_ol_L);
    //imshow(ol_LS.c_str(), op_ol_LS);
    //imshow(ol_R.c_str(), op_ol_R);
    //imshow(ol_RS.c_str(), op_ol_RS);



    cerr << "Overlap Areas Found " << overlaps_CV.size() << endl;

    vector<float> results_CV_QT = quantitativeTestingWithHomography(overlaps_CV[0], overlaps_CV[2], overlaps_CV[1], overlaps_CV[3]);

    cerr << "Results from Quantitative Testing :" << endl;
    cerr << "Geometric Quaity :" << results_CV_QT[0] << endl;
    cerr << "Photometric Quality : (Color) : " << results_CV_QT[1] << endl;
    cerr << "Photometric Quality : (Intensity) : " << results_CV_QT[2] << endl << endl;

    cerr << results_CV_QT[0] << " " << results_CV_QT[1] << " " << results_CV_QT[2] << endl;

    vector<double> results_CV_UQI_L = universalQualityIndex(overlaps_CV[0], overlaps_CV[1]);
    vector<double> results_CV_UQI_R = universalQualityIndex(overlaps_CV[2], overlaps_CV[3]);

    cerr << "Results from Unviersal Quality Index : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "B: " << results_CV_UQI_L[0] << " G: " << results_CV_UQI_L[1] << " R: " << results_CV_UQI_L[2] << endl;
    cerr << "Overall : " << results_CV_UQI_L[3] << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "B: " << results_CV_UQI_R[0] << " G: " << results_CV_UQI_R[1] << " R: " << results_CV_UQI_R[2] << endl;
    cerr << "Overall : " << results_CV_UQI_R[3] << endl << endl;

    // Implement IQA

    vector<float> results_CV_IQA_L = imageQualityAssessmentHomography(overlaps_CV[0], overlaps_CV[1]);
    vector<float> results_CV_IQA_R = imageQualityAssessmentHomography(overlaps_CV[2], overlaps_CV[3]);

    cerr << "Results from IQA : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "SSIM: " << results_CV_IQA_L[0] << " MSSIM: " << results_CV_IQA_L[1];
    cerr << "Right vs. Stitched" << endl;
    cerr << "SSIM: " << results_CV_IQA_R[0] << " MSSIM: " << results_CV_IQA_R[1];

    cout << output_no << ",OpenCV,," << results_CV_IQA_L[0] << "," << results_CV_IQA_R[0] << "," << results_CV_IQA_L[1] <<"," <<results_CV_IQA_R[1] << "," ;
    cout << results_CV_QT[0] << "," << results_CV_QT[1] << "," << results_CV_QT[2] << "," ;
    cout << results_CV_UQI_L[0] << ","<< results_CV_UQI_L[1] << ","<< results_CV_UQI_L[2] << ","<< results_CV_UQI_L[3] << "," ;
    cout << results_CV_UQI_R[0] << ","<< results_CV_UQI_R[1] << ","<< results_CV_UQI_R[2] << ","<< results_CV_UQI_R[3] << "," ;
    cout << duration_CV << endl;

    cerr <<  "Over" << endl;

    return;
}

void nonlinear_blending_test(Mat image1, Mat image2, char * output_no){

    vector <Mat> results_NLB;

    high_resolution_clock::time_point t1_NLB = high_resolution_clock::now();
    results_NLB = nonLinearBlending(image1, image2);
    high_resolution_clock::time_point t2_NLB = high_resolution_clock::now();

    auto duration_NLB = std::chrono::duration_cast<std::chrono::microseconds>( t2_NLB - t1_NLB ).count();


    cerr << "Stitched Successfully with Non Linear Blending Stitching : time : " << duration_NLB << "microseconds" << endl;

    Mat stitched_NLB = results_NLB[0];
    Mat homography_NLB = results_NLB[1];

    string save_name2 (output_no);
    save_name2 += "_NLBStitching.jpg";
    imwrite(save_name2.c_str(), stitched_NLB);

    cerr << "Stitched Image Saved to : " << save_name2 << endl << endl;

    vector<Mat> overlaps_NLB = OverlapAreas(image2, image1, stitched_NLB, homography_NLB);

    /*string ol_L (output_no);
    ol_L += "_NLB_ol_L";
    string ol_LS (output_no);
    ol_LS += "_NLB_ol_LS";
    string ol_R (output_no);
    ol_R += "_NLB_ol_R";
    string ol_RS (output_no);
    ol_RS += "_NLB_ol_RS";*/

    //imshow(ol_L.c_str(), overlaps_NLB[0]);
    //imshow(ol_LS.c_str(), overlaps_NLB[1]);
    //imshow(ol_R.c_str(), overlaps_NLB[2]);
    //imshow(ol_RS.c_str(), overlaps_NLB[3]);

    //waitKey(0);


    cerr << "Overlap Areas Found " << overlaps_NLB.size() << endl;

    vector<float> results_NLB_QT = quantitativeTestingWithHomography(overlaps_NLB[0], overlaps_NLB[2], overlaps_NLB[1], overlaps_NLB[3]);

    cerr << "Results from Quantitative Testing :" << endl;
    cerr << "Geometric Quaity :" << results_NLB_QT[0] << endl;
    cerr << "Photometric Quality : (Color) : " << results_NLB_QT[1] << endl;
    cerr << "Photometric Quality : (Intensity) : " << results_NLB_QT[2] << endl << endl;

    //cout << results_NLB_QT[0] << " " << results_NLB_QT[1] << " " << results_NLB_QT[2] << endl;

    vector<double> results_NLB_UQI_L = universalQualityIndex(overlaps_NLB[0], overlaps_NLB[1]);
    vector<double> results_NLB_UQI_R = universalQualityIndex(overlaps_NLB[2], overlaps_NLB[3]);

    cerr << "Results from Unviersal Quality Index : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "B: " << results_NLB_UQI_L[0] << " G: " << results_NLB_UQI_L[1] << " R: " << results_NLB_UQI_L[2] << endl;
    cerr << "Overall : " << results_NLB_UQI_L[3] << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "B: " << results_NLB_UQI_R[0] << " G: " << results_NLB_UQI_R[1] << " R: " << results_NLB_UQI_R[2] << endl;
    cerr << "Overall : " << results_NLB_UQI_R[3] << endl << endl;

    // Implement IQA


    vector<float> results_NLB_IQA_L = imageQualityAssessmentHomography(overlaps_NLB[0], overlaps_NLB[1]);
    vector<float> results_NLB_IQA_R = imageQualityAssessmentHomography(overlaps_NLB[2], overlaps_NLB[3]);
    
    cerr << "Results from IQA : " << endl;
    cerr << "Left vs. Stitched" << endl;
    cerr << "SSIM: " << results_NLB_IQA_L[0] << " MSSIM: " << results_NLB_IQA_L[1]  << endl;
    cerr << "Right vs. Stitched" << endl;
    cerr << "SSIM: " << results_NLB_IQA_R[0] << " MSSIM: " << results_NLB_IQA_R[1] <<  endl;

    cout << output_no << ",NLB,," << results_NLB_IQA_L[0] << "," << results_NLB_IQA_R[0] << "," << results_NLB_IQA_L[1] <<"," <<results_NLB_IQA_R[1] << "," ;
    cout << results_NLB_QT[0] << "," << results_NLB_QT[1] << "," << results_NLB_QT[2] << "," ;
    cout << results_NLB_UQI_L[0] << ","<< results_NLB_UQI_L[1] << ","<< results_NLB_UQI_L[2] << ","<< results_NLB_UQI_L[3] << "," ;
    cout << results_NLB_UQI_R[0] << ","<< results_NLB_UQI_R[1] << ","<< results_NLB_UQI_R[2] << ","<< results_NLB_UQI_R[3] << "," ;
    cout << duration_NLB << endl;
}

int main(int argc, char ** argv){
    if(argc < 2){
        readme();
        return 0;
    }

    Mat image1 = imread(argv[1]);
    Mat image2 = imread(argv[2]);

    char * output_no;

    output_no = argv[3];
    //else output_no = "1";
    cerr << "Loaded " << argv[1] << " and " << argv[2] << endl << endl;


    fast_stitching_test(image1.clone(), image2.clone(), output_no);
    harris_stitching_test(image1.clone(), image2.clone(), output_no);
    opencv_stitching_test(image1.clone(), image2.clone(), output_no);
    nonlinear_blending_test(image1.clone(), image2.clone(), output_no);

    return 0;
}

void readme(){
    cout << "Usage: $ ./FYP.o </path/to/image1> </path/to/image2> <serial_no>[optional, as a prefix for output images]" << endl;
}
