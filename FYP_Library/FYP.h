//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#ifndef FYP_FYP_H
#define FYP_FYP_H

#include "Alignment/Alignment.h"
#include "Fast_Stitcing/FastStitching.H"
#include "Image_Quality_Assessment/ImageQualityAssessment.h"
#include "Quantitative_Testing/QuantitativeTesting.h"
#include "Harris_Stitching/HarrisStitching.h"
#include "OverlapWithHomography/OverlapWithHomography.h"
#include "Universal_Quality_Index/UniversalQualityIndex.h"
#include "Non_Linear_Blending/NonLinearBlending.h"
#include "CV_Overlap/CVOverlap.h"

#endif //FYP_FYP_H
