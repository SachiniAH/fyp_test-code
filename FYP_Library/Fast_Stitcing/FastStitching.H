//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#ifndef FYP_FASTSTITCHING_H
#define FYP_FASTSTITCHING_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>


using namespace cv;
using namespace std;

Mat fastStitching(Mat image1, Mat image2, vector<float> overlap_percentages);

#endif //FYP_FASTSTITCHING_H
