#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <cmath>
#include "../FYP.h"

using namespace std;
using namespace cv;

Mat getWindow(Mat inImg, int i, int j, int num){

            Rect region_of_interest = Rect(i, j, num, num);
            return inImg(region_of_interest);

}

/*Mat splitLeftImg(Mat inImg, int percentage){
    int startx = inImg.cols * (100 - percentage) / 100;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty,  inImg.cols - startx, inImg.rows);
    return inImg(region_of_interest);
}

Mat splitRightImg(Mat inImg, int percentage){
    int startx = 0;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty, inImg.cols * percentage / 100, inImg.rows);
    return inImg(region_of_interest);
}

Mat splitStitchedImg(Mat inImg, int percentage, int img1_cols, int img2_cols){
    int startx = img1_cols * (100 - percentage) / 100;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty, img2_cols * percentage / 100, inImg.rows);
    return inImg(region_of_interest);
}*/

float findSigma2(Mat img, Mat img2){
    float sum;
    float num_pixels = 0;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            if(img.at<Vec3b>(i, j)[0] +  img.at<Vec3b>(i, j)[1] + img.at<Vec3b>(i, j)[2] +
                    img2.at<Vec3b>(i, j)[0] +  img2.at<Vec3b>(i, j)[1] + img2.at<Vec3b>(i, j)[2] > 0){
                sum += ( img.at<Vec3b>(i, j)[0] * img2.at<Vec3b>(i, j)[0]  +
                        img.at<Vec3b>(i, j)[1] * img2.at<Vec3b>(i, j)[1]  +
                        img.at<Vec3b>(i, j)[2] * img2.at<Vec3b>(i, j)[2]);
                num_pixels += 1.0f;
            }
        }
    }
    //return sum/(((img.cols * img.rows)-1)*1.0);
    if( num_pixels > 0)
        return sum / num_pixels;
    else return 0;
}

float findSigma(Mat img){
    float sum;
    float num_pixels = 0;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            if(pow(img.at<Vec3b>(i, j)[0],2) + pow(img.at<Vec3b>(i, j)[1],2) + pow(img.at<Vec3b>(i, j)[2],2) > 0) {
                sum += (pow(img.at<Vec3b>(i, j)[0], 2) + pow(img.at<Vec3b>(i, j)[1], 2) +
                        pow(img.at<Vec3b>(i, j)[2], 2));
                num_pixels += 1.0f;
            }
        }
    }
    //return sqrt(sum/(((img.cols * img.rows)-1)*1.0));
    if ( num_pixels > 0)
        return sqrt(sum/num_pixels);
    else return 0;
}

float findMu(Mat img){
    float sum;
    float num_pixels = 0;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            if(img.at<Vec3b>(i, j)[0] + img.at<Vec3b>(i, j)[1] + img.at<Vec3b>(i, j)[2] > 0) {
                sum += (img.at<Vec3b>(i, j)[0] + img.at<Vec3b>(i, j)[1] + img.at<Vec3b>(i, j)[2]);
                num_pixels += 1.0f;
            }
        }
    }
    //return sum/(img.cols * img.rows * 3.0);
    if(num_pixels > 0)
        return sum/num_pixels;
    else return 0;
}


Mat removeIntensity(Mat img, float intensity){
    uchar val = (uchar)round(intensity);
    //cout << "val " << val << endl;
    for(int i=0; i<img.rows; ++i){
        for(int j=0; j<img.cols; ++j){
            if(img.at<Vec3b>(i,j)[0] + img.at<Vec3b>(i,j)[1] + img.at<Vec3b>(i,j)[2] > 0) {
                img.at<Vec3b>(i, j)[0] = abs(img.at<Vec3b>(i, j)[0] - val);
                img.at<Vec3b>(i, j)[1] = abs(img.at<Vec3b>(i, j)[1] - val);
                img.at<Vec3b>(i, j)[2] = abs(img.at<Vec3b>(i, j)[2] - val);
            }
            //cout << "[" << i << "," << j << "] ";
        }
        //cout << endl;
    }
    return img;
}


float SSIM(Mat imgX, Mat imgY, int C1=0, int C2=0){
    float muX = findMu(imgX);
    float muY = findMu(imgY);

    //cout << "Mu " << muX << " " << muY << endl;

    Mat newX = removeIntensity(imgX, muX);
    Mat newY = removeIntensity(imgY, muY);

    //cout << "Removing Intensity Done" << endl;

    float sigmaX = findSigma(newX);
    float sigmaY = findSigma(newY);

    //cout << "Sigma " << sigmaX << " " << sigmaY << endl;

    float sigmaXY = findSigma2(newX, newY);

    //cout << "SigmaXY " << sigmaXY << endl;

    float SSIM = ((2*muX*muY + C1)*(2*sigmaXY + C2)) / ((muX*muX + muY*muY + C1)*(sigmaX*sigmaX + sigmaY*sigmaY + C2));
    if( (2*muX*muY + C1)*(2*sigmaXY + C2) == 0 || (muX*muX + muY*muY + C1)*(sigmaX*sigmaX + sigmaY*sigmaY + C2) == 0 ){
        return 0;
    }
    return SSIM;
}


double MSSIM_2(Mat img1, Mat img2, int n){
    int C1 = 0, C2 = 0;
    double MSSIM = 0.0;
    double count = 0;


    for (int i = 0; i < img1.cols - n; i++){
        for (int j = 0; j < img1.rows - n; j++){
            double xbar = 0, ybar = 0, xvar = 0, yvar = 0, cov = 0;
            bool skip = false;
            for (int k = i; k < i + n; k++){
                for (int l = j; l < j + n; l++){
                    if (img1.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) == 0 || img2.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) == 0){
                        skip = true;
                        //black
                        break;
                    }
                    xbar += img1.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 });
                    ybar += img2.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 });

                }
                if (skip)break;
            }
            if (skip)continue;
            xbar /= (n*n);
            ybar /= (n*n);
            for (int k = i; k < i + n; k++){
                for (int l = j; l < j + n; l++){

                    xvar += pow((img1.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) - 1 * xbar), 2);
                    yvar += pow((img2.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) - 1 * ybar), 2);
                    cov += (img1.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) - 1 * xbar)*(img2.at<Vec3b>(Point(k, l)).dot({ 1, 1, 1 }) - 1 * ybar);


                }

            }
            xvar /= (n*n);
            yvar /= (n*n);
            cov /= (n*n);
            double SSIM = ((2 * xbar*ybar + C1)*(2 * cov + C2)) / ((xbar*xbar + ybar*ybar + C1)*(xvar + yvar + C2));
            //((2 * muX*muY + C1)*(2 * sigmaXY + C2)) / ((muX*muX + muY*muY + C1)*(sigmaX*sigmaX + sigmaY*sigmaY + C2));
            if (SSIM != SSIM)continue;
            MSSIM += SSIM;
            count++;
        }
    }

    return (MSSIM / count);
}

float MSSIM(Mat imgX, Mat imgY, int num){

    Mat temp;
    Mat temp2;

    //imgX = temp2; imgY = temp; // To prevent assertion error.

    //bilateralFilter(imgX, temp, 18,150,150);
    //bilateralFilter(imgY, temp2, 18, 150, 150);

    //imgX = temp;
    //imgY = temp;

    float num_windows = 0;

    float sum = 0;
    for(int i=0; i< min(imgX.rows, imgY.rows) - num; ++i){
        for(int j=0; j< min(imgX.cols, imgY.cols) - num; ++j) {
            //cout << i << " " << j << endl;
            float temp = SSIM(getWindow(imgX,j, i, num ), getWindow(imgY, j, i, num));
            sum += temp;
            if(temp > 0)
                num_windows ++;
            //cout << sum << endl;
        }
    }
    //return sum/((min(imgX.rows, imgY.rows) - num)*(min (imgX.cols, imgY.cols) - num) * 1.0);
    if(num_windows > 0){
        return sum / num_windows;
    }
    else return 0;
}

vector<float> imageQualityAssessmentHomography(Mat image1, Mat image2){
    float val1 = SSIM(image1, image2);
    float val2 = MSSIM(image1, image2, 8);

    vector<float> result = {val1, val2};

    return result;
}

vector<float> imageQualityAssessment( Mat _leftImg, Mat _rightImg, Mat _stitchedImg, float perc ){

    int percentage = (int)round(perc);

    Mat leftImg = splitLeftImg(_leftImg, percentage);
    Mat rightImg = splitRightImg(_rightImg, percentage);
    Mat stitchedImg = splitStitchedImg(_stitchedImg, percentage, leftImg.cols, rightImg.cols);

    //cout << "Splitting Done" << endl;

    float val1 = SSIM(leftImg, stitchedImg);
    float val2 = SSIM(rightImg, stitchedImg);

    float newval1 = MSSIM(leftImg, stitchedImg, 8);
    float newval2 = MSSIM(rightImg, stitchedImg, 8);

    //cout << "SSIM : Left vs Stitched : " << val1 << endl;
    //cout << "SSIM : Right vs Stitched : " << val2 << endl;

    //cout << "MSSIM : Left vs Stitched : " << newval1 << endl;
    //cout << "MSSIM : Right vs Stitched : " << newval2 << endl;

    vector<float> result = {val1, val2, newval1, newval2};

    return result;
}