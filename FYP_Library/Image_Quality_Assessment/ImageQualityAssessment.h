//
// Created by Rajith Vidanaarachchi on 22/6/15.
//

#ifndef FYP_IMAGEQUALITYASSESSMENT_H
#define FYP_IMAGEQUALITYASSESSMENT_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

vector<float> imageQualityAssessment( Mat leftImg, Mat rightImg, Mat stitchedImg, float perc );
vector<float> imageQualityAssessmentHomography(Mat image1, Mat image2);

#endif //FYP_IMAGEQUALITYASSESSMENT_H
