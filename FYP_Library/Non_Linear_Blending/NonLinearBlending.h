//
// Created by Rajith Vidanaarachchi on 29/6/15.
//

#ifndef FYP_NONLINEARBLENDING_H
#define FYP_NONLINEARBLENDING_H

#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

vector<Mat> nonLinearBlending(Mat image1, Mat image2);

#endif //FYP_NONLINEARBLENDING_H
