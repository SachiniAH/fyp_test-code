//
// Created by Sachith Seneviratne on 27/6/15.
//
#include <stdio.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;


/** @function nonlinearBlending */
vector<Mat> nonLinearBlending(Mat image1, Mat image2)
{

    Mat img_object;
    Mat img_scene;
    Mat img_object1 = image1;
    Mat img_scene1 = image2;

    cvtColor(image1, img_object, CV_RGB2GRAY);
    cvtColor(image2, img_scene, CV_RGB2GRAY);


    //-- Step 1: Detect the keypoints using SIFT Detector
    int minHessian = 400;

    SiftFeatureDetector detector(minHessian);

    std::vector<KeyPoint> keypoints_object, keypoints_scene;

    detector.detect(img_object, keypoints_object);
    detector.detect(img_scene, keypoints_scene);

    //-- Step 2: Calculate descriptors (feature vectors)
    SiftDescriptorExtractor extractor;

    Mat descriptors_object, descriptors_scene;

    extractor.compute(img_object, keypoints_object, descriptors_object);
    extractor.compute(img_scene, keypoints_scene, descriptors_scene);

    //-- Step 3: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match(descriptors_object, descriptors_scene, matches);

    double max_dist = 0; double min_dist = 100;
    vector <double> dists;
    //-- Quick calculation of max and min distances between keypoints
    for (int i = 0; i < descriptors_object.rows; i++)
    {
        double dist = matches[i].distance;
        dists.push_back(matches[i].distance);
        if (dist < min_dist) min_dist = dist;
        if (dist > max_dist) max_dist = dist;
    }

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;

    for (int i = 0; i < descriptors_object.rows; i++)
    {
        if (matches[i].distance < 3 * min_dist)
        {
            good_matches.push_back(matches[i]);
        }
    }

    Mat img_matches;
    drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
                good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    //-- Localize the object
    std::vector<Point2f> obj;
    std::vector<Point2f> scene;

    for (int i = 0; i < good_matches.size(); i++)
    {
        //-- Get the keypoints from the good matches
        obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
        scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
    }

    //Mat H = findHomography(scene, obj, CV_RANSAC,2);
    Mat H = findHomography(scene, obj, CV_RANSAC);

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(img_object.cols, 0);
    obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);
    std::vector<Point2f> scene_corners(4);

    Mat C;
    //perspectiveTransform(img_object, C, H);
    warpPerspective(img_scene1, C, H, cv::Size(img_object1.cols + img_scene1.cols, img_object1.rows));


    Size size(800, 600);//the dst image size,e.g.100x100
    Mat dst;//dst image
    //cv::Mat half(C, cv::Rect(0, 0, img_object1.cols, img_object1.rows));
    //img_object1.copyTo(half);


    int H1 = C.rows;
    int W = C.cols*0.75;
    double alpha;
    int T = 1200;
    //std::cout << "T is " << T << std::endl;
    for (int y = 0; y<C.rows; y++)
    {
        for (int x = 0; x<C.cols; x++)
        {
            // get pixel
            Vec3b color = C.at<Vec3b>(Point(x, y));
            Vec3b color2;
            if (!(x >= img_object1.cols || y >= img_object1.rows)){
                color2 = img_object1.at<Vec3b>(Point(x, y));
                // ... do something to the color ....

                if (min(x, min(y, min(abs(x - W), abs(y - H1))))>T)alpha = 1;
                else alpha = (sin(atan(1) * 4 * (min(x, min(y, min(abs(x - W), abs(y - H1)))) / T) - 0.5) + 1) / 2;
                // alpha = .5;
                color = (1 - alpha)*color + alpha*color2;
                if (C.at<Vec3b>(Point(x, y)).dot({ 1, 1, 1 }) == 0)color = color2;

                // set pixel
            }

            C.at<Vec3b>(Point(x, y)) = color;
        }
    }
    vector<Mat> results = { C, H };

    /*
    Size sz(800, 600);
    resize(C, C, sz);
    imshow("Result", C);
    */
    return results;
}