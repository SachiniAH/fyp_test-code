#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <cmath>

using namespace std;
using namespace cv;

Mat getWindow(Mat inImg, int i, int j, int num){
    //for(int i=0; i< inImg.rows - num; ++i){
    //    for(int j=0; j< inImg.cols - num; ++j){
            Rect region_of_interest = Rect(i, j, num, num);
            return inImg(region_of_interest);

    //    }
    //}
}

Mat splitLeftImg(Mat inImg, int percentage){
    int startx = inImg.cols * (100 - percentage) / 100;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty,  inImg.cols - startx, inImg.rows);
    return inImg(region_of_interest);
}

Mat splitRightImg(Mat inImg, int percentage){
    int startx = 0;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty, inImg.cols * percentage / 100, inImg.rows);
    return inImg(region_of_interest);
}

Mat splitStitchedImg(Mat inImg, int percentage, int img1_cols, int img2_cols){
    int startx = img1_cols * (100 - percentage) / 100;
    int starty = 0;
    Rect region_of_interest = Rect(startx, starty, img2_cols * percentage / 100, inImg.rows);
    return inImg(region_of_interest);
}

float findSigma2(Mat img, Mat img2){
    float sum;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            sum += ( img.at<Vec3b>(i, j)[0] * img2.at<Vec3b>(i, j)[0]  +
                     img.at<Vec3b>(i, j)[1] * img2.at<Vec3b>(i, j)[1]  +
                     img.at<Vec3b>(i, j)[2] * img2.at<Vec3b>(i, j)[2]);
        }
    }
    return sum/(((img.cols * img.rows)-1)*1.0);
}

float findSigma(Mat img){
    float sum;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            sum += (pow(img.at<Vec3b>(i, j)[0],2) + pow(img.at<Vec3b>(i, j)[1],2) + pow(img.at<Vec3b>(i, j)[2],2));
        }
    }
    return sqrt(sum/(((img.cols * img.rows)-1)*1.0));
}

float findMu(Mat img){
    float sum;
    for(int i=0; i<img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            sum += (img.at<Vec3b>(i, j)[0] + img.at<Vec3b>(i, j)[1] + img.at<Vec3b>(i, j)[2]);
        }
    }
    return sum/(img.cols * img.rows * 3.0);
}


Mat removeIntensity(Mat img, float intensity){
    uchar val = (uchar)round(intensity);
    //cout << "val " << val << endl;
    for(int i=0; i<img.rows; ++i){
        for(int j=0; j<img.cols; ++j){
            img.at<Vec3b>(i,j)[0] = abs(img.at<Vec3b>(i,j)[0] - val);
            img.at<Vec3b>(i,j)[1] = abs(img.at<Vec3b>(i,j)[1] - val);
            img.at<Vec3b>(i,j)[2] = abs(img.at<Vec3b>(i,j)[2] - val);
            //cout << "[" << i << "," << j << "] ";
        }
        //cout << endl;
    }
    return img;
}


float SSIM(Mat imgX, Mat imgY, int C1=0, int C2=0){
    float muX = findMu(imgX);
    float muY = findMu(imgY);

    cout << "Mu " << muX << " " << muY << endl;

    Mat newX = removeIntensity(imgX, muX);
    Mat newY = removeIntensity(imgY, muY);

    cout << "Removing Intensity Done" << endl;

    float sigmaX = findSigma(newX);
    float sigmaY = findSigma(newY);

    cout << "Sigma " << sigmaX << " " << sigmaY << endl;

    float sigmaXY = findSigma2(newX, newY);

    cout << "SigmaXY " << sigmaXY << endl;

    float SSIM = ((2*muX*muY + C1)*(2*sigmaXY + C2)) / ((muX*muX + muY*muY + C1)*(sigmaX*sigmaX + sigmaY*sigmaY + C2));

    return SSIM;
}

float MSSIM(Mat imgX, Mat imgY, int num){

    Mat temp = imgX;
    Mat temp2 = imgY;

    bilateralFilter(temp, imgX,18,150,150);
    bilateralFilter(temp2, imgY, 18, 150, 150);

    float sum = 0;
    for(int i=0; i< imgX.rows - num; ++i){
        for(int j=0; j< imgX.cols - num; ++j) {
            sum += SSIM(getWindow(imgX,i, j, num ), getWindow(imgY, i, j, num));
        }
    }
    return sum/((imgX.rows-num)*(imgX.cols-num));
}


vector<Mat> images;

int main( int argc, char** argv ){

    if( argc < 4 ){
        printf( " No image data \n " );
        return -1;
    }


    for(int i = 1; i<argc; ++i){
        images.push_back (imread( argv[i], 1));
    }

    int percentage = atoi(argv[4]);

    Mat leftImg = splitLeftImg(images[0], percentage);
    Mat rightImg = splitRightImg(images[1], percentage);
    Mat stitchedImg = splitStitchedImg(images[2], percentage, images[0].cols, images[1].cols);

    cout << "Splitting Done" << endl;

    float val1 = SSIM(leftImg, stitchedImg);
    float val2 = SSIM(rightImg, stitchedImg);

    float newval1 = MSSIM(leftImg, stitchedImg, 8);
    float newval2 = MSSIM(rightImg, stitchedImg, 8);

    cout << "SSIM : Left vs Stitched : " << val1 << endl;
    cout << "SSIM : Right vs Stitched : " << val2 << endl;

    cout << "MSSIM : Left vs Stitched : " << newval1 << endl;
    cout << "MSSIM : Right vs Stitched : " << newval2 << endl;

    return 0;
}