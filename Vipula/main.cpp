#include "opencv2/opencv.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "queue"

using namespace cv;
using namespace std;

vector<KeyPoint> detectKeypoints(Mat image, int M, int N, int n_corners);

Mat computeDescriptor(Mat image, vector<KeyPoint> keypoints);

vector<DMatch> getMatchingPoints(Mat descriptor1, Mat descriptor2);

double claculateMean(Mat image);

double calculateVariance(Mat &image, double mean);

double sigmoid(double varience, double min_variance, double max_varience, double exp_variance, double ratio_param);

vector<KeyPoint> goodFeaturesToTrackModified(Mat image, int maxCorners, double qualityLevel, double minDistance,
                                             const _InputArray &_mask, int blockSize, double harrisK);

vector<DMatch> matchPoints(Mat image1, vector<KeyPoint> keypoints1, Mat image2, vector<KeyPoint> keypoints2);

double intensityAroundCorner(KeyPoint point, Mat gray_image);

set<pair<int, int>> intersec(set<pair<int, int>> set1, set<pair<int, int>> set2);

bool isAnElement(int n, int i, set<pair<int, int>> set1);

set<pair<int, int>> unionOf(set<pair<int, int>> set1, set<pair<int, int>> set2);

float calculateLength(Point2f point1, Point2f point2, double width);

double calculateAngle(Point2f point1, Point2f point2);

class Compare {
public:
    bool operator()(KeyPoint kp1, KeyPoint kp2) {
        return kp1.size < kp2.size;
    }
};

template<typename T>
struct greaterThanPtr {
    bool operator()(const T *a, const T *b) const { return *a > *b; }
};

int main() {

    Mat image1, image2;
    image2 = imread("../test/4.jpg");
    image1 = imread("../test/3.jpg");

    if (!image1.data || !image2.data) {
        cout << "Error Loading Images." << endl;
        return 1;
    }

    cout << "image 1 size :" << image1.rows << "x" << image1.cols << endl;
    cout << "image 2 size :" << image2.rows << "x" << image2.cols << endl;

    imshow("Image 1", image1);
    imshow("Image 2", image2);
    // waitKey(0);

    int keypoint_limit = 300;
    vector<KeyPoint> keypoints1, keypoints2;
    keypoints1 = detectKeypoints(image1, 5, 5, keypoint_limit);
    keypoints2 = detectKeypoints(image2, 5, 5, keypoint_limit);

    for (int j = 0; j < keypoints1.size(); ++j) {
        //circle(image1, Point((int) keypoints1[j].pt.x, (int) keypoints1[j].pt.y), 5, Scalar(0, 255, 255), 2, 8, 0);
    }
    for (int j = 0; j < keypoints2.size(); ++j) {
        //circle(image2, Point((int) keypoints2[j].pt.x, (int) keypoints2[j].pt.y), 5, Scalar(0, 255, 255), 2, 8, 0);
    }

   // imshow("kp1 ",image1);
   // imshow("kp2",image2);
    waitKey(0);

    Mat descriptor1, descriptor2;
    try {
        descriptor1 = computeDescriptor(image1, keypoints1);
        descriptor2 = computeDescriptor(image2, keypoints2);
    } catch (Exception e) {
        return 1;
    }

    vector<DMatch> matches = matchPoints(image1, keypoints1, image2, keypoints2);

    // vector<DMatch> matches;
    // matches = getMatchingPoints(descriptor1, descriptor2);

    double max_dist = 0;
    double min_dist = 100;


//-- Quick calculation of max and min distances between key points

    for (int i = 0; i < matches.size(); i++) {
        double dist = matches[i].distance;
        if (dist < min_dist) min_dist = dist;
        if (dist > max_dist) max_dist = dist;
    }

//-- Use only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector<DMatch> good_matches;
    double threshold = 3 * min_dist;
    for (int i = 0; i < matches.size(); i++) {
        double distance = matches[i].distance;
        if (distance < threshold) {
            good_matches.push_back(matches[i]);

        }
    }

    cout << "found good matches: " << good_matches.size() << " / " << matches.size() << endl;
    if (good_matches.size() == 0) {
        cout << "No good matches found..!!" << endl;
        return 1;
    }

    std::vector<Point2f> selected_points1;
    std::vector<Point2f> selected_points2;

    for (int i = 0; i < good_matches.size(); i++) {
        //-- Get the keypoints from the good matches
        selected_points1.push_back(keypoints1[good_matches[i].queryIdx].pt);
        circle(image1, Point((int) keypoints1[good_matches[i].queryIdx].pt.x, (int) keypoints1[good_matches[i].queryIdx].pt.y), 5,
               Scalar(0, 255, 255), 2, 8, 0);

        selected_points2.push_back(keypoints2[good_matches[i].trainIdx].pt);

        circle(image2, Point((int) keypoints2[good_matches[i].trainIdx].pt.x, (int) keypoints2[good_matches[i].trainIdx].pt.y), 5,
               Scalar(0, 255, 255), 2, 8, 0);
    }

    imshow("Image 1", image1);
    imshow("Image 2", image2);
    //waitKey(0);

// Find the Homography Matrix

    //cout << "f factor :( : " << selected_points1.size() << " & " << selected_points2.size() << endl;
    Mat H = findHomography(selected_points2, selected_points1, CV_RANSAC, 10);

    Mat image_matches;
    drawMatches(image1, keypoints1, image2, keypoints2, good_matches, image_matches, Scalar::all(-1), Scalar::all(-1), vector<char>(),
                DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    imshow("matches ", image_matches);

    // Use the Homography Matrix to warp the images
    cv::Mat result;
    warpPerspective(image1, result, H, cv::Size(image1.cols + image2.cols, image1.rows));
    //warpAffine(image1, result, H, cv::Size(image1.cols + image2.cols, image1.rows));
    cv::Mat half(result, cv::Rect(0, 0, image2.cols, image2.rows));
    image1.copyTo(half);

    imshow("Result", result);

    waitKey(0);
    return 0;
}

vector<DMatch> matchPoints(Mat image1, vector<KeyPoint> keypoints1, Mat image2, vector<KeyPoint> keypoints2) {
    cout << keypoints1.size() << " keypoints found in image 1." << keypoints2.size() << " keypoints found in image 2" <<
    endl;

    double n_threshold = .75;

    double max_r;
    for (int i = 0; i < keypoints1.size(); ++i) {
        if (keypoints1.at(i).response > max_r) {
            max_r = keypoints1.at(i).response;
        }
    }
    for (int i = 0; i < keypoints2.size(); ++i) {
        if (keypoints2.at(i).response > max_r) {
            max_r = keypoints2.at(i).response;
        }
    }
    double r_threshold = 0.05 * max_r;
    double y_threshold = image1.rows / 3;

    int window_size = 3;

    Mat gray_img1, gray_img2;

    cvtColor(image1, gray_img1, CV_RGB2GRAY);
    cvtColor(image2, gray_img2, CV_RGB2GRAY);

    double similarity_mat[keypoints1.size()][keypoints2.size()];

    double avg_img1;
    double tmp = 0;
    for (int i = 0; i < keypoints1.size(); i++) {
        tmp += intensityAroundCorner(keypoints1.at(i), gray_img1);
    }
    avg_img1 = tmp / keypoints1.size();

    double avg_img2;
    tmp = 0;
    for (int i = 0; i < keypoints2.size(); i++) {
        tmp += intensityAroundCorner(keypoints2.at(i), gray_img2);
    }
    avg_img2 = tmp / keypoints2.size();

    for (int i = 0; i < keypoints1.size(); i++) {
        KeyPoint kp1 = keypoints1.at(i);
        for (int j = 0; j < keypoints2.size(); j++) {
            KeyPoint kp2 = keypoints2.at(j);

            if (abs(kp1.pt.y - kp2.pt.y) < y_threshold && kp1.pt.x > kp2.pt.y &&
                abs(kp1.response - kp2.response) < r_threshold) {

                KeyPoint left = kp1;
                KeyPoint right = kp2;
                double corr_lr = 0, corr_ll = 0, corr_rr = 0, ncc = 0;
                for (int k = -window_size; k < window_size; k++) {
                    for (int l = -window_size; l < window_size; l++) {
                        left.pt.x += k;
                        left.pt.y += l;
                        right.pt.x += k;
                        right.pt.y += l;
                        double val_l = intensityAroundCorner(left, gray_img1) - avg_img1;
                        double val_r = intensityAroundCorner(right, gray_img2) - avg_img2;

                        corr_ll += val_l * val_l;
                        corr_lr += val_l * val_r;
                        corr_rr += val_r * val_r;

                    }
                }

                ncc = corr_lr / sqrt(corr_rr * corr_ll);
                double abs_ncc = abs(ncc);
                if (abs_ncc > n_threshold) {
                    similarity_mat[i][j] = abs_ncc;
                }
            }
        }
    }

    set<pair<int, int>> selected_pairs_l;

    for (int m = 0; m < keypoints1.size(); m++) {
        double max_ncc = 0;
        int max_index = -1;
        for (int i = 0; i < keypoints2.size(); i++) {
            if (max_ncc < similarity_mat[m][i]) {
                max_ncc = similarity_mat[m][i];
                max_index = i;
            }
        }
        pair<int, int> max_pair(m, max_index);
        selected_pairs_l.insert(max_pair);
    }

    set<pair<int, int>> selected_pairs_r;

    for (int m = 0; m < keypoints2.size(); m++) {
        double max_ncc = 0;
        int max_index = -1;
        for (int i = 0; i < keypoints1.size(); i++) {
            if (max_ncc < similarity_mat[i][m]) {
                max_ncc = similarity_mat[i][m];
                max_index = i;
            }
        }
        pair<int, int> max_pair(m, max_index);
        selected_pairs_r.insert(max_pair);
    }

    set<pair<int, int >> t_l;
    for (set<pair<int, int>>::iterator it = selected_pairs_l.begin(); it != selected_pairs_l.end(); ++it) {

        if (it->second > 0) {
            pair<int, int> sel_pair(it->first, it->second);
            t_l.insert(sel_pair);
        }
    }

    set<pair<int, int >> t_r;
    for (set<pair<int, int>>::iterator it = selected_pairs_r.begin(); it != selected_pairs_r.end(); ++it) {

        if (it->second > 0) {
            pair<int, int> sel_pair(it->second, it->first);
            t_r.insert(sel_pair);
        }
    }

    set<pair<int, int>> intersection = intersec(t_l, t_r);
    set<pair<int, int>> union_l_r = unionOf(t_l, t_r);

    cout << "intersection : " << intersection.size() << endl;
    cout << "union : " << union_l_r.size() << endl;

    vector<DMatch> ret;

    double sim_start[keypoints1.size()][keypoints2.size()];
    for (int n = 0; n < keypoints1.size(); ++n) {
        for (int i = 0; i < keypoints2.size(); ++i) {
            if (isAnElement(n, i, union_l_r)) {
                sim_start[n][i] = 1.0;

            } else {
                sim_start[n][i] = similarity_mat[n][i];
            }
        }
    }

    double angle_threshold = 1.5;
    float length_threshold = image1.cols / 2;

    double D[keypoints1.size()][keypoints2.size()];
    for (int i = 0; i < keypoints1.size(); ++i) {
        for (int j = 0; j < keypoints2.size(); ++j) {
            float length = calculateLength(keypoints1.at(i).pt, keypoints2.at(j).pt, image1.cols);
            double ang = abs(calculateAngle(keypoints1.at(i).pt, keypoints2.at(j).pt));

            double r_diff = abs(keypoints1.at(i).response - keypoints2.at(j).response);
            if (length < length_threshold && ang < angle_threshold && r_diff < r_threshold) {
                // cout << " 16 constraints " << length << " " << ang << " " << r_diff << endl;
                D[i][j] = (sim_start[i][j] + sim_start[j][i]) / 2.0;

                DMatch match(i, j, D[i][j] + .5);
                ret.push_back(match);
            } else {
                D[i][j] = 0;
            }
        }
    }

    return ret;

}

double calculateAngle(Point2f point1, Point2f point2) {
    return atan((point1.y - point2.y) / (point1.x - point2.x)) * 180 / 3.14159265;
}

float calculateLength(Point2f point1, Point2f point2, double width) {
    //return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
    return abs(point2.x + width - point1.x);
}

set<pair<int, int>> unionOf(set<pair<int, int>> set1, set<pair<int, int>> set2) {
    set<pair<int, int>> ret;
    for (set<pair<int, int>>::iterator it = set1.begin(); it != set1.end(); ++it) {
        ret.insert(pair<int, int>(it->first, it->second));

    }
    for (set<pair<int, int>>::iterator it = set2.begin(); it != set2.end(); ++it) {
        ret.insert(pair<int, int>(it->first, it->second));
    }
    return ret;
}

bool isAnElement(int n, int i, set<pair<int, int>> set1) {
    for (set<pair<int, int>>::iterator it = set1.begin(); it != set1.end(); ++it) {
        if (it->first == n && it->second == i)
            return true;
    }
    return false;
}

set<pair<int, int>> intersec(set<pair<int, int>> set1, set<pair<int, int>> set2) {
    set<pair<int, int>> ret;
    for (set<pair<int, int>>::iterator it = set1.begin(); it != set1.end(); ++it) {
        for (set<pair<int, int>>::iterator it2 = set2.begin(); it2 != set2.end(); ++it2) {
            if (it->first == it2->first && it->second == it2->second) {
                pair<int, int> sel(it->first, it->second);
                ret.insert(sel);
            }
        }

    }
    return ret;
}

double intensityAroundCorner(KeyPoint point, Mat gray_image) {
    int n_points = 1;
    double tot = 0;
    int x = (int) point.pt.x;
    int y = (int) point.pt.y;

    tot += gray_image.at<double>(x, y);

    if (x > 0) {
        ++n_points;
        tot += gray_image.at<double>(x - 1, y);

        if (y > 0) {
            ++n_points;
            tot += gray_image.at<double>(x - 1, y - 1);
        }

    }
    if (y > 0) {
        ++n_points;
        tot += gray_image.at<double>(x, y - 1);
    }

    if (y < gray_image.rows) {
        ++n_points;
        tot += gray_image.at<double>(x, y + 1);
    }
    if (y < gray_image.cols) {
        ++n_points;
        tot += gray_image.at<double>(x + 1, y);
        if (y < gray_image.rows) {
            ++n_points;
            tot += gray_image.at<double>(x + 1, y + 1);
        }
    }

    return tot / n_points;
}

vector<DMatch> getMatchingPoints(Mat descriptor1, Mat descriptor2) {
    vector<DMatch> matches;

    FlannBasedMatcher matcher;
    matcher.match(descriptor1, descriptor2, matches);

    return matches;
}

Mat computeDescriptor(Mat image, vector<KeyPoint> keypoints) {
    Mat descriptor;

    SurfDescriptorExtractor extractor;

    try {
        extractor.compute(image, keypoints, descriptor);
    } catch (Exception e) {
        cout << "error while computing descriptor : " << e.msg << endl;

    }
    return descriptor;
}

vector<KeyPoint> detectKeypoints(Mat image, int M, int N, int n_corners) {

    Mat gray;
    cvtColor(image, gray, CV_BGR2GRAY);

    int blockWidth = image.cols / M;
    int blockHeight = image.rows / N;

    vector<Mat> blocks;

    for (int i = 0; i < image.cols; i += blockWidth) {
        for (int j = 0; j < image.rows; j += blockHeight) {
            Rect rect = Rect(i, j, blockWidth, blockHeight);
            blocks.push_back(Mat(gray, rect));
        }
    }

    vector<double> grayscale_variance;

    for (int k = 0; k < blocks.size(); k++) {
        double mean = claculateMean(blocks[k]);
        double varience = calculateVariance(blocks[k], mean);
        // cout << "mean of block " << k << " : " << mean << endl;
        // cout << "varience of block " << k << " : " << varience << endl;
        grayscale_variance.push_back(varience);
    }

    double min_variance = 10, max_varience = 0, sum_var = 0, exp_variance;

    for (int l = 0; l < grayscale_variance.size(); l++) {
        if (grayscale_variance[l] > max_varience) { max_varience = grayscale_variance[l]; }
        if (grayscale_variance[l] < min_variance) { min_variance = grayscale_variance[l]; }

        sum_var += grayscale_variance[l];
    }
    exp_variance = sum_var / grayscale_variance.size();

    vector<double> grayscale_varience_norm;
    double ratio_param = 10;
    double sum_normalized = 0;
    for (int i = 0; i < grayscale_variance.size(); i++) {
        double normalized = sigmoid(grayscale_variance[i], min_variance, max_varience, exp_variance, ratio_param);
        grayscale_varience_norm.push_back(normalized);
        sum_normalized += normalized;
        //   cout << "normalized varience of block " << i << " : " << normalized << endl;
    }

    vector<double> texture_weights;
    for (int i = 0; i < grayscale_varience_norm.size(); i++) {
        double weight = grayscale_varience_norm.at(i) / sum_normalized;
        texture_weights.push_back(weight);
        //cout << "weight of block " << i << " : " << weight << endl;
    }

    vector<int> corners_in_region;
    for (int i = 0; i < texture_weights.size(); i++) {
        int corners = texture_weights[i] * n_corners;
        corners_in_region.push_back(corners);
        cout << "corners of block " << i << " : " << corners << endl;
    }

    vector<KeyPoint> ret;
    /* for (int i = 0; i < blocks.size(); i++) {
         Mat block = blocks.at(i);
         vector<KeyPoint> block_keypoints;
         block_keypoints.empty();
         Ptr<FeatureDetector> detector = FeatureDetector::create("GFTT");
         detector->set("useHarrisDetector", true);
         detector->detect(block, block_keypoints);

         Mat dst, dst_norm, dst_norm_scaled;
         cornerHarris(gray, dst, 2, 3, 0.04);
         normalize(dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
         convertScaleAbs(dst_norm, dst_norm_scaled);
         priority_queue<KeyPoint, vector<KeyPoint>, Compare> keypoint_queue;

         int x = i % M * blockWidth;
         int y = i / M * blockHeight;

         keypoint_queue.empty();
         for (int j = 0; j < block.cols; j++) {
             for (int k = 0; k < block.rows; k++) {
                 KeyPoint keyPoint = KeyPoint(x + j, y + k, dst_norm.at<float>(j, i));
                 keypoint_queue.push(keyPoint);
             }
         }


         int limit = corners_in_region.at(i);
         cout << "selected " << limit << " corners in block " << i << endl;
         for (int j = 0; j < limit; j++) {

             KeyPoint keyPoint = keypoint_queue.top();


             cout << "size " << keyPoint.size << " x " << keyPoint.pt.x << " y " << keyPoint.pt.y << endl;

             ret.push_back(keyPoint);
             keypoint_queue.pop();
         }

     }
           */
    ret.empty();

    for (int i = 0; i < blocks.size(); i++) {
        Mat block = blocks.at(i);
        vector<KeyPoint> corners;

        int max_corners = corners_in_region.at(i);
        if (max_corners < 1) { max_corners = 1; }

        double qualityLevel = 0.01;
        double minDistance = 2;
        int blockSize = 4;
        double k = 0.04;

        /// Copy the source image
        Mat copy;
        copy = block.clone();

        /// Apply corner detection
        corners = goodFeaturesToTrackModified(block, max_corners, qualityLevel, minDistance, Mat(), blockSize, k);
        int x = i % M * blockWidth;
        int y = i / M * blockHeight;

        //   cout << "block " << i << endl;
        for (int m = 0; m < corners.size(); ++m) {
            KeyPoint keyPoint = corners.at(m);
            keyPoint.pt.x += x;
            keyPoint.pt.y += y;

            //cout << m << " x: " << keyPoint.pt.x << " y : " << keyPoint.pt.y << " value " << keyPoint.response << endl;
            ret.push_back(keyPoint);
        }

    }

    cout << ret.size() << " keypoints found" << endl;

    Ptr<FeatureDetector> detector = FeatureDetector::create("GFTT");
    detector->set("useHarrisDetector", true);
    vector<KeyPoint> original_harris_out;
    detector->detect(gray, original_harris_out);

    return ret;
}

double sigmoid(double varience, double min_variance, double max_varience, double exp_variance, double ratio_param) {
    double tmp = (varience - exp_variance) / (max_varience - min_variance);
    double tmp2 = exp((-1) * ratio_param * tmp) + 1;
    return 1 / tmp2;
}

double calculateVariance(Mat &image, double mean) {
    double sum = 0;
    for (int i = 0; i < image.cols; i++) {
        for (int j = 0; j < image.rows; j++) {
            double value = image.at<uchar>(i, j);
            sum += pow((value - mean), 2);
        }
    }

    double var = sum / image.rows * image.cols;
    return var;
}

double claculateMean(Mat image) {
    double sum = 0;

    for (int i = 0; i < image.cols; ++i) {
        for (int j = 0; j < image.rows; ++j) {
            sum += image.at<uchar>(i, j);
        }
    }
    int pixcels = image.rows * image.cols;

    return sum / pixcels;

}

vector<KeyPoint> goodFeaturesToTrackModified(Mat image, int maxCorners, double qualityLevel, double minDistance, const _InputArray &_mask,
                                             int blockSize, double harrisK) {

    Mat mask = _mask.getMat();

    CV_Assert(qualityLevel > 0 && minDistance >= 0 && maxCorners >= 0);
    CV_Assert(mask.empty() || (mask.type() == CV_8UC1 && mask.size() == image.size()));

    Mat eig, tmp;

    cornerHarris(image, eig, blockSize, 3, harrisK);

    double maxVal = 0;
    minMaxLoc(eig, 0, &maxVal, 0, 0, mask);
    threshold(eig, eig, maxVal * qualityLevel, 0, THRESH_TOZERO);
    dilate(eig, tmp, Mat());

    Size imgsize = image.size();

    vector<const float *> tmpCorners;

    // collect list of pointers to features - put them into temporary image
    for (int y = 1; y < imgsize.height - 1; y++) {
        const float *eig_data = (const float *) eig.ptr(y);
        const float *tmp_data = (const float *) tmp.ptr(y);
        const uchar *mask_data = mask.data ? mask.ptr(y) : 0;

        for (int x = 1; x < imgsize.width - 1; x++) {
            float val = eig_data[x];
            if (val != 0 && val == tmp_data[x] && (!mask_data || mask_data[x]))
                tmpCorners.push_back(eig_data + x);
        }
    }

    sort(tmpCorners, greaterThanPtr<float>());
    vector<KeyPoint> corners;
    size_t i, j, total = tmpCorners.size(), ncorners = 0;

    if (minDistance >= 1) {
        // Partition the image into larger grids
        int w = image.cols;
        int h = image.rows;

        const int cell_size = cvRound(minDistance);
        const int grid_width = (w + cell_size - 1) / cell_size;
        const int grid_height = (h + cell_size - 1) / cell_size;

        std::vector<std::vector<Point2f> > grid((unsigned long) grid_width * grid_height);

        minDistance *= minDistance;

        for (i = 0; i < total; i++) {
            int ofs = (int) ((const uchar *) tmpCorners[i] - eig.data);
            int y = (int) (ofs / eig.step);
            int x = (int) ((ofs - y * eig.step) / sizeof(float));

            bool good = true;

            int x_cell = x / cell_size;
            int y_cell = y / cell_size;

            int x1 = x_cell - 1;
            int y1 = y_cell - 1;
            int x2 = x_cell + 1;
            int y2 = y_cell + 1;

            // boundary check
            x1 = std::max(0, x1);
            y1 = std::max(0, y1);
            x2 = std::min(grid_width - 1, x2);
            y2 = std::min(grid_height - 1, y2);

            for (int yy = y1; yy <= y2; yy++) {
                for (int xx = x1; xx <= x2; xx++) {
                    vector<Point2f> &m = grid[yy * grid_width + xx];

                    if (m.size()) {
                        for (j = 0; j < m.size(); j++) {
                            float dx = x - m[j].x;
                            float dy = y - m[j].y;

                            if (dx * dx + dy * dy < minDistance) {
                                good = false;
                                goto break_out;
                            }
                        }
                    }
                }
            }

            break_out:

            if (good) {
                // printf("%d: %d %d -> %d %d, %d, %d -- %d %d %d %d, %d %d, c=%d\n",
                //    i,x, y, x_cell, y_cell, (int)minDistance, cell_size,x1,y1,x2,y2, grid_width,grid_height,c);
                grid[y_cell * grid_width + x_cell].push_back(Point2f((float) x, (float) y));
                KeyPoint keyPoint(Point2f((float) x, (float) y), 0, -1, *tmpCorners[i]);
                corners.push_back(keyPoint);
                ++ncorners;

                if (maxCorners > 0 && (int) ncorners == maxCorners)
                    break;
            }
        }
    }
    else {
        for (i = 0; i < total; i++) {
            int ofs = (int) ((const uchar *) tmpCorners[i] - eig.data);
            int y = (int) (ofs / eig.step);
            int x = (int) ((ofs - y * eig.step) / sizeof(float));

            KeyPoint keyPoint(Point2f((float) x, (float) y), 0, -1, *tmpCorners[i]);
            corners.push_back(keyPoint);
            ++ncorners;
            if (maxCorners > 0 && (int) ncorners == maxCorners)
                break;
        }
    }

    return corners;

}
