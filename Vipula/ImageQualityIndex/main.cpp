#include "opencv2/opencv.hpp"

using namespace std;

vector<double> calculateQuality(cv::Mat original_image, cv::Mat secondary_image);

vector<double> calculateMeanIntensity(vector<cv::Vec3b> pixels_vector);

vector<double> calculateStandardDiv(vector<cv::Vec3b> pixels_vector, vector<double> mean);

vector<double> calculateCombinedStandardDiv(vector<cv::Vec3b> pixels_original, vector<cv::Vec3b> pixels_secondary, vector<double> mean_original,
                                            vector<double> mean_secondary);

bool isBlack(cv::Vec3b first, cv::Vec3b sec);

int main() {
    cv::Mat original_image, secondary_image;

    original_image = cv::imread("./lena.bmp");
    secondary_image = cv::imread("./lena2.jpg");

    if (!original_image.data || !secondary_image.data) {
        cout << "Error while loading images !" << endl;
        return 1;
    }

    vector<double> quality = calculateQuality(original_image, secondary_image);

    cout << "Quality od Image Red : " << quality[2] << " Green : " << quality[1] << " Blue : " << quality[0] <<
    endl;

    double Q = .299f * quality[2] + .587f * quality[1] + .114f * quality[0];
    cout << "Quality factor : " << Q << endl;

}

vector<double> calculateQuality(cv::Mat original_image, cv::Mat secondary_image) {

    //since both images considered as in similar dimensions, this width and height will used in secondary image too.
    int width = original_image.cols;
    int height = original_image.rows;

    vector<cv::Vec3b> pixels_original, pixels_secondary;
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            cv::Vec3b pixel_or = original_image.at<cv::Vec3b>(i, j);
            cv::Vec3b pixel_sec = secondary_image.at<cv::Vec3b>(i, j);

            if (!isBlack(pixel_or, pixel_sec)) {
                pixels_original.push_back(pixel_or);
                pixels_secondary.push_back(pixel_sec);
            }
        }
    }

    vector<double> mean_original, mean_secondary, sd_original, sd_secondary, sd_both;

    mean_original = calculateMeanIntensity(pixels_original);
    mean_secondary = calculateMeanIntensity(pixels_secondary);

    cout << "Mean intensity of Original Image Red : " << mean_original[2] << " Green : " << mean_original[1] << " Blue : " << mean_original[0] <<
    endl;
    cout << "Mean intensity of Original Image Red : " << mean_secondary[2] << " Green : " << mean_secondary[1] << " Blue : " << mean_secondary[0] <<
    endl;

    sd_original = calculateStandardDiv(pixels_original, mean_original);
    sd_secondary = calculateStandardDiv(pixels_secondary, mean_secondary);

    cout << "SD of intensity of Original Image Red : " << (float) sd_original[2] << " Green : " << (float) sd_original[1] << " Blue : " <<
    (float) sd_original[0] << endl;
    cout << "SD of intensity of Original Image Red : " << (float) sd_secondary[2] << " Green : " << (float) sd_secondary[1] <<
    " Blue : " << (float) sd_secondary[0] << endl;

    sd_both = calculateCombinedStandardDiv(pixels_original, pixels_secondary, mean_original, mean_secondary);
    cout << "combined SD of intensity  Red : " << (float) sd_both[2] << " Green : " << (float) sd_both[1] << " Blue : " <<
    (float) sd_both[0] << endl;

    double q_red = (4 * (float) sd_both[2] * (float) mean_original[2] * (float) mean_secondary[2]) /
                   (((float) sd_original[2] + (float) sd_secondary[2]) *
                    (pow((float) mean_original[2], 2) + pow((float) mean_secondary[2], 2)));

    double q_green = (4 * (float) sd_both[1] * (float) mean_original[1] * (float) mean_secondary[1]) /
                     (((float) sd_original[1] + (float) sd_secondary[1]) *
                      (pow((float) mean_original[1], 2) + pow((float) mean_secondary[1], 2)));

    double q_blue = (4 * (float) sd_both[0] * (float) mean_original[0] * (float) mean_secondary[0]) /
                    (((float) sd_original[0] + (float) sd_secondary[0]) *
                     (pow((float) mean_original[0], 2) + pow((float) mean_secondary[0], 2)));

    vector<double> ret;
    ret.push_back(q_blue);
    ret.push_back(q_green);
    ret.push_back(q_red);
    return ret;
}

bool isBlack(cv::Vec3b first, cv::Vec3b sec) {
    if (((float) first[2]+(float) first[1]+(float) first[0])<1 &&((float) sec[2]+(float) sec[1]+(float) sec[0])<1 ) {
        return true;
    }
    return false;
}

vector<double> calculateCombinedStandardDiv(vector<cv::Vec3b> pixels_original, vector<cv::Vec3b> pixels_secondary, vector<double> mean_original,
                                            vector<double> mean_secondary) {
    double red_difference = 0, green_difference = 0, blue_difference = 0;
    for (int i = 0; i < pixels_original.size(); ++i) {
        cv::Vec3b original_pixel = pixels_original.at(i);
        cv::Vec3b secondary_pixel = pixels_secondary.at(i);
        red_difference +=
                ((float) original_pixel[2] - (float) mean_original[2]) * ((float) secondary_pixel[2] - (float) mean_secondary[2]);
        green_difference +=
                ((float) original_pixel[1] - (float) mean_original[1]) * ((float) secondary_pixel[1] - (float) mean_secondary[1]);
        blue_difference +=
                ((float) original_pixel[0] - (float) mean_original[0]) * ((float) secondary_pixel[0] - (float) mean_secondary[0]);

    }

    int n = pixels_original.size();
    vector<double> ret;
    ret.push_back(blue_difference / (n - 1));
    ret.push_back(green_difference / (n - 1));
    ret.push_back(red_difference / (n - 1));
    return ret;
}

vector<double> calculateStandardDiv(vector<cv::Vec3b> pixels_vector, vector<double> mean) {
    double red_square_difference = 0, green_square_difference = 0, blue_square_difference = 0;
    for (int i = 0; i < pixels_vector.size(); ++i) {
        cv::Vec3b pixel = pixels_vector.at(i);
        red_square_difference += pow(((double) pixel.val[2] - mean[2]), 2);
        green_square_difference += pow(((double) pixel.val[1] - mean[1]), 2);
        blue_square_difference += pow(((double) pixel.val[0] - mean[0]), 2);
    }

    int n = pixels_vector.size();
    vector<double> ret;
    ret.push_back(blue_square_difference / (n - 1));
    ret.push_back(green_square_difference / (n - 1));
    ret.push_back(red_square_difference / (n - 1));
    return ret;
}

/**
 * Calculate Mean intensity of each pixels, calculate for RGB channels seperately.
 */
vector<double> calculateMeanIntensity(vector<cv::Vec3b> pixels_vector) {

    double total_red = 0, total_green = 0, total_blue = 0;

    for (int i = 0; i < pixels_vector.size(); ++i) {
        cv::Vec3b pixel = pixels_vector.at(i);
        total_red += (double) pixel.val[2];
        total_green += (double) pixel.val[1];
        total_blue += (double) pixel.val[0];

    }
    double n = pixels_vector.size();
    vector<double> ret;
    ret.push_back(total_blue / n);
    ret.push_back(total_green / n);
    ret.push_back(total_red / n);
    return ret;
}